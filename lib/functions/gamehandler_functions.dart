import 'dart:math';
import 'package:flutter_math_fork/flutter_math.dart';


class GameHandlerFunctions {

  Random random = new Random();
  String wert = '0';
  bool aB, bB, mB;
  int a = 0, b = 0, m = 1 ;
  List<int> mValList = [-3,-1,1,3];

  int fakea = 0, fakeb = 0 , fakem = 1;
  int realButton = 1;
  bool exerciseiscorrect = true;
  List<Math> buttonList = [];

  void init(){
    print(aB?'a is true':'a no true');
    print(bB?'b is true':'b no true');
    print(mB?'m is true':'m no true');
      aB?a = random.nextInt(14) - 7:a=0;
      bB?b = random.nextInt(14) - 7:b=0;
      mB?m = mValList[random.nextInt(mValList.length)]:m=1;
      exerciseiscorrect = random.nextBool();
  }

  void initFalseNum() {
    if (aB) {
      fakea = random.nextInt(14) - 7;
      if (a == fakeb) {
        fakea += random.nextInt(5) - 7;
      }
    }
    if (bB) {
      fakeb = random.nextInt(14) - 7;
      if (b == fakeb) {
        fakeb += random.nextInt(5) + 1;
      }
    }
    if (mB) {
      fakem = random.nextInt(14) - 7;
      if (m == fakem) {
        fakem += random.nextInt(14) - 7;
      }
      if (fakem == 0) {
        fakem += random.nextInt(14) - 7;
      }
    }
  }

  void initFourButton() {
    realButton = random.nextInt(3) + 1;
    for (int i = 1; i <= 4; i++) {
      if (realButton == i) {
        buttonList.add(Math.tex(r'\rm{f(x)=  ' + mStr(true) + aStr(true) + bStr(true) + '}'));
      } else {
        buttonList.add(Math.tex(r'\rm{f(x)=  ' + mStr(false) + aStr(false) + bStr(false) + '}'));
      }
    }
  }


  String aStr(bool correct) {
    //a must be negative but not worth time see functionlvlcreator
    String bufferStr;
    String returnStr = '';
    if (correct) {
      bufferStr = a.toString();
      if (a < 0) {
        returnStr = '(x' + bufferStr + ')^2';
      } else if (a == 0) {
        returnStr = 'x^2';
      } else {
        returnStr = '(x+' + bufferStr + ')^2';
      }
    } else {
      initFalseNum();
      bufferStr = fakea.toString();
      if (fakea < 0) {
        returnStr = '(x^2' + bufferStr + ')';
      } else if (fakea == 0) {
        returnStr = 'x^2';
      } else {
        returnStr = '(x^2+' + bufferStr + ')';
      }
    }
    return returnStr;
  }

  String bStr(bool correct) {
    String bufferStr;
    String returnStr = '';
    if (correct) {
      bufferStr = b.toString();
      if (b < 0) {
        returnStr = bufferStr;
      } else if (b == 0) {
        returnStr = '';
      } else {
        returnStr = '+ ' + bufferStr;
      }
    } else {
      initFalseNum();
      bufferStr = fakeb.toString();
      if (fakeb < 0) {
        returnStr = bufferStr;
      } else if (fakeb == 0) {
        returnStr = '';
      } else {
        returnStr = '+ ' + bufferStr;
      }
    }
    return returnStr;
  }

  String mStr(bool correct) {
    String bufferStr;
    String returnStr = '';
    if (correct) {
      bufferStr = m.toString();
      if (m < 0) {
        returnStr = bufferStr;
      } else if (m == 1) {
        returnStr = '';
      } else if (m == -1) {
        returnStr = '-';
      } else {
        returnStr = bufferStr;
      }
    } else {
      initFalseNum();
      bufferStr = fakem.toString();
      if (fakem < 0) {
        returnStr = bufferStr;
      } else if (fakem == 1) {
        returnStr = '';
      }else if (m == -1) {
        returnStr = '-';
      } else {
        returnStr = bufferStr;
      }
    }
    return returnStr;
  }
}
