import 'package:flutter/cupertino.dart';
import 'package:math_trainer/activities/menus/afterlevel_screen.dart';
import 'package:math_trainer/data/data_public_variables.dart';

import 'shared_prefs.dart';

class GameFunctions {
  void nextGameLevels(List<List<Widget>> gameList) {
    if (SharedPrefs()
            .getSpInt(currentTopic + currentTopicIndex.toString() + 'Level') <=
        gameList[currentTopicIndex].length) {
      SharedPrefs().increaseSpInt(currentTopic + '_percentage', 1);
    }
    SharedPrefs().increaseSpInt(
        currentTopic + currentTopicIndex.toString() + 'Level', 1);
    runApp(AfterLevelScreen(levelList: gameList));
  }
}
