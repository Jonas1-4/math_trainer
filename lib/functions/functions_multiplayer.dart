import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:math_trainer/activities/multiplayer/mp_game_handler.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

import 'shared_prefs.dart';

class MultiplayerFunctions {
  Color color = color_background_main[theme];
  String profilePic;
  String score;
  Map<dynamic, dynamic> foundGamesMap;
  Map<dynamic, dynamic> oppMap = {};
  ImageProvider oppPp, ownPp;List<String> mpScoreList;

  //Stream documentStream =
  DatabaseReference database = FirebaseDatabase.instance.reference();
  String uid = FirebaseAuth.instance.currentUser.uid;

  setSearching(bool b) {
    SharedPrefs().setSpBool('isSearching', b);
  }

  //GOES through map of found Games and checks if player 1 || 2 is assigned to uid
  // if player 1 generate game else wait for generated game.
  //also adds Players to FoundGamesList and Opponent Sharedprefs
  onGameFound() {
    List<String> gameIDs = SharedPrefs().getSpStringList(spGameIDs);
    if (foundGamesMap != null) {
      for (dynamic i in foundGamesMap.keys) {
        print('i:' + i.toString());
        if (!gameIDs.contains(i)) {
          if (foundGamesMap[i]['player1'] == uid || foundGamesMap[i]['player2'] == uid) {
            SharedPrefs().setSpBool('isSearching', false);
            database.child('Games').child(i).once().then((DataSnapshot data) {
              database
                  .child('Users')
                  .child(foundGamesMap[i]['player1'] == uid ? data.value['player2'] : data.value['player1'])
                  .once()
                  .then((DataSnapshot user) {
                oppMap = user.value;
                setSearching(false);
                mpScoreList = SharedPrefs().getSpStringList(spPlayedGames + i);
                if (foundGamesMap[i]['player1'] == uid) {
                  //if player host && game not generated, generate and add player card
                  SharedPrefs().setSpStr('opponent' + i, foundGamesMap[i]['player2']);
                  print('isHost');
                  if (data.value['GameList'] == null) {
                    MpGamehandler().generateMpGame(database.child('Games').child(i).child('GameList'));
                  }
                  if (!gameIDs.contains(i)) {
                    SharedPrefs().addSpStringList(spGameIDs, i);
                    print('\nHost added Game\n');

                    SharedPrefs().addSpStringList(
                        spFoundGames,
                        jsonEncode({
                          'image': oppMap["photoURL"],
                          'displayName': oppMap["displayName"],
                          'gameID': i,
                          'opponentPlayed': mpScoreList.length == 5 &&
                              foundGamesMap[i][SharedPrefs().getSpStr('opponent' + i)] != null,
                        }));
                  }
                } else {
                  //if (foundGamesMap[i]['player2'] == null && foundGamesMap[i]['GameList'] != null) {
                  print('isGuest');
                  //if player !host wait for generated game add player card
                  SharedPrefs().setSpStr('opponent' + i, foundGamesMap[i]['player1']);
                  if (!gameIDs.contains(i)) {
                    print('\nGuest added Game\n');
                    SharedPrefs().addSpStringList(
                        spFoundGames,
                        jsonEncode({
                          'image': oppMap["photoURL"],
                          'displayName': oppMap["displayName"],
                          'gameID': i,
                          'opponentPlayed': mpScoreList.length == 5 &&
                              foundGamesMap[i][SharedPrefs().getSpStr('opponent' + i)] != null,
                        }));
                    SharedPrefs().addSpStringList(spGameIDs, i);
                  }
                }
              });
            });
          }
        }
      }
    }
  }
}
