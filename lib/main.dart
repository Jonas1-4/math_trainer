import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_trainer/functions/shared_prefs.dart';

import 'activities/menus/home_screen.dart';
import 'functions/bloc_observer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // TODO add Firebase for Web and desktop
  // (Firebase realtime Database noWeb support yet)
  if(Platform.isAndroid  || Platform.isIOS){
    await Firebase.initializeApp();
  }
  await SystemChrome.setEnabledSystemUIOverlays([]);
  await SharedPrefs().init();
  
  Bloc.observer = MathTrainerObserver();
  //no Portrait mode 
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MaterialApp(
            title: 'Math trainer',
            initialRoute: '/',
            routes: {
              '/': (context) =>  HomeScreen(),
            }));
  });
}


