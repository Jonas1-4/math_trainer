import 'dart:ui';

import 'package:flutter/cupertino.dart';

class KeyboardLevelModel {
  KeyboardLevelModel({
    this.color,
    this.operatorStr,
    this.numLimit,
    this.numLimitList,
    this.maxLevel,
    this.minus,
  });

  final Color color;
  final bool minus;
  final int numLimit, maxLevel;
  final List<int> numLimitList;
  final String operatorStr;
  List<int> exerciseNumbers = [];
  int score = 0, solution;
  double solutionFractions;
  String input = '0', exercise;
  reset() {
    input = '0';
    exerciseNumbers = [];
  }
}
