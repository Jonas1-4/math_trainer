import 'dart:ui';

import 'package:flutter/material.dart';

Color darken(Color c, [int percent = 10]) {
  assert(1 <= percent && percent <= 100);
  var f = 1 - percent / 100;
  return Color.fromARGB(c.alpha, (c.red * f).round(), (c.green * f).round(),
      (c.blue * f).round());
}

Color brighten(Color c, [int percent = 10]) {
  assert(1 <= percent && percent <= 100);
  var p = percent / 100;
  return Color.fromARGB(
      c.alpha,
      c.red + ((255 - c.red) * p).round(),
      c.green + ((255 - c.green) * p).round(),
      c.blue + ((255 - c.blue) * p).round());
}

List<Color> color_background_main = [Colors.white, Color(0xFF282828)];
List<Color> color_background_main_title = [Colors.blueGrey[900], Color(0xFFd79921)
];
List<Color> color_background_main_subtitle = [Colors.grey, Color(0xFFb57614)];

//Topicsection
List<List<Color>> color_themes = [
  [Colors.blueAccent, Color(0xFF458588)],
  [Colors.teal, Color(0xFFb16286)],
  [Colors.orange, Color(0xFFd79921)],
  [Colors.deepPurple, Color(0xFF689d68)],
];

List<Color> color_background_topics_1 = [Colors.blueAccent, Color(0xFF458588)];
List<Color> color_background_topics_2 = [Colors.teal, Color(0xFFb16286)];
List<Color> color_background_topics_3 = [Colors.orange, Color(0xFFd79921)];
List<Color> color_background_topics_4 = [Colors.deepPurple, Color(0xFF689d68)];

List<Color> color_background_topics_accent = [Colors.white, Color(0xFFebdbb2)];
List<Color> color_backgound_topicsubtitle = [
  Colors.blueGrey,
  Color(0xaFF89984)
];

List<Color> color_hints = [Colors.grey, Color(0xdda89984)];
List<Color> color_wrong = [Colors.red, Color(0xFFcc241d)];
List<Color> color_right = [Colors.green, Color(0xFF98971a)];
