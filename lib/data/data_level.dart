import 'package:flutter/cupertino.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:math_trainer/activities/basics/basic_init.dart';
import 'package:math_trainer/activities/fractions/fractions_init.dart';
import 'package:math_trainer/activities/functions/FunctionsLevelCreator.dart';
import 'package:math_trainer/activities/functions/FunctionsPracticeCreator.dart';
import 'package:math_trainer/activities/menus/topic_select.dart';
import 'package:math_trainer/activities/power/power_screen.dart';
import 'package:math_trainer/widgets/function_graph_menu.dart';
import 'data_color.dart';
import 'data_public_variables.dart';



List<TopicPageViewItem> listBasic = [
	TopicPageViewItem(color: color_background_topics_1[theme], subTitle: 'Learn how to add one number to another.', topic: Math.tex('1+3=?'),  app: basicsLevelList[0], trainingmode: basicsPracticeList, ),
	TopicPageViewItem(color: color_background_topics_2[theme], subTitle: 'Learn how to substract one number from another.', topic: Math.tex('1-3=?'),  app: basicsLevelList[1], trainingmode: basicsPracticeList,),
TopicPageViewItem(color: color_background_topics_3[theme], subTitle: 'Learn how to multiply one number with another.', topic: Math.tex('1*3=?'),  app: basicsLevelList[2], trainingmode: basicsPracticeList,),
TopicPageViewItem(color: color_background_topics_4[theme], subTitle: 'Learn how to divide one number from another.', topic: Math.tex('1:3=?'),  app: basicsLevelList[3], trainingmode: basicsPracticeList,),
];

List<List<Widget>>  basicsLevelList= [[
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimit: 10  , operatorStr: '+', maxLevel: 3),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimit: 20  , operatorStr: '+', maxLevel: 6),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimit: 100 , operatorStr: '+', maxLevel: 5),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimit: 1000, operatorStr: '+', maxLevel: 5),
],
 [
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_2[theme], numLimit: 10  , operatorStr: '-', maxLevel: 4),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_2[theme], numLimit: 20  , operatorStr: '-', maxLevel: 6),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_2[theme], numLimit: 100 , operatorStr: '-', maxLevel: 5, minus: true,),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_2[theme], numLimit: 1000, operatorStr: '-', maxLevel: 5, minus: true,),
],
 [
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_3[theme], numLimit: 5 , operatorStr: '*', maxLevel: 3),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_3[theme], numLimit: 10, operatorStr: '*', maxLevel: 5),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_3[theme], numLimit: 15, operatorStr: '*', maxLevel: 5),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_3[theme], numLimit: 25, operatorStr: '*', maxLevel: 5),
],
 [
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_4[theme], numLimit: 5 , operatorStr: ':', maxLevel: 3),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_4[theme], numLimit: 10, operatorStr: ':', maxLevel: 5),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_4[theme], numLimit: 20, operatorStr: ':', maxLevel: 4),
	BasicInit().createBasicScreen(gameMode: 'Normal', color: color_background_topics_4[theme], numLimit: 30, operatorStr: ':', maxLevel: 4),
],
];
List<List<Widget>>  basicsPracticeList= [[
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimit: 10  , operatorStr: '+', ),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimit: 20  , operatorStr: '+', ),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimit: 100 , operatorStr: '+',),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimit: 1000, operatorStr: '+',),
],
 [
  BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_2[theme], numLimit: 10  , operatorStr: '-', ),
  BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_2[theme], numLimit: 20  , operatorStr: '-', ),
  BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_2[theme], numLimit: 100 , operatorStr: '-', minus: true,),
  BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_2[theme], numLimit: 1000, operatorStr: '-', minus: true,),
],
 [
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_3[theme], numLimit: 5 , operatorStr: '*', ),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_3[theme], numLimit: 10, operatorStr: '*',),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_3[theme], numLimit: 15, operatorStr: '*',),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_3[theme], numLimit: 25, operatorStr: '*',),
],
 [
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_4[theme], numLimit: 5 , operatorStr: ':', ),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_4[theme], numLimit: 10, operatorStr: ':', ),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_4[theme], numLimit: 20, operatorStr: ':', ),
	BasicInit().createBasicScreen(gameMode: 'Practice', color: color_background_topics_4[theme], numLimit: 30, operatorStr: ':', ),
],
];


List<TopicPageViewItem> listFractions = [
	TopicPageViewItem(color: color_background_topics_1[theme], subTitle: 'Learn how to add one fraction to another.', topic: Math.tex(r'\frac{1}{3}+\frac{3}{1}= ?'),  app: fractionsLevelList[0], trainingmode: fractionsPracticeList,),
	TopicPageViewItem(color: color_background_topics_2[theme], subTitle: 'Learn how to substract one fraction from another.', topic: Math.tex(r'\frac{1}{3}-\frac{3}{1}= ?'),  app: fractionsLevelList[1], trainingmode: fractionsPracticeList,),
	TopicPageViewItem(color: color_background_topics_3[theme], subTitle: 'Learn how to multiply fraction.',topic:Math.tex(r'\frac{1}{3}*\frac{3}{1}= ?'),   app: fractionsLevelList[2], trainingmode: fractionsPracticeList,),
	TopicPageViewItem(color: color_background_topics_4[theme], subTitle: 'Learn how to divide fractions.',topic:Math.tex(r'\frac{1}{3}:\frac{3}{1}= ?'),    app: fractionsLevelList[3], trainingmode: fractionsPracticeList,),
];

List<List<Widget>>  fractionsLevelList= [[
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 2, operatorStr: '+', maxLevel: 5),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 4, operatorStr: '+', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 6, operatorStr: '+', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 20, numLimitBot: 9, operatorStr: '+', maxLevel: 3),
],                                                          
 [                                                          
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 2, operatorStr: '-', maxLevel: 5),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 4, operatorStr: '-', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 6, operatorStr: '-', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 20, numLimitBot: 9, operatorStr: '-', maxLevel: 3),
],                                                          
 [                                                          
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 6 , operatorStr: '*', maxLevel: 5),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 10, operatorStr: '*', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 15, operatorStr: '*', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 25, numLimitBot: 25, operatorStr: '*', maxLevel: 3),
],                                                          
 [                                                          
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 6 , operatorStr: ':', maxLevel: 5),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 10, operatorStr: ':', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 15, operatorStr: ':', maxLevel: 4),
	FractionInit().createFractionScreen(gameMode: 'Normal', color: color_background_topics_1[theme], numLimitTop: 25, numLimitBot: 25, operatorStr: ':', maxLevel: 3),
],
];

List<List<Widget>>  fractionsPracticeList= [[
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 2, operatorStr: '+', ),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 4, operatorStr: '+', ),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 6, operatorStr: '+', ),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 20, numLimitBot: 9, operatorStr: '+', ),
],                                                            
 [                                                            
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 2, operatorStr: '-', ),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 4, operatorStr: '-', ),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 6, operatorStr: '-', ),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 20, numLimitBot: 9, operatorStr: '-', ),
],                                                            
 [                                                            
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 6 , operatorStr: '*',),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 10, operatorStr: '*',),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 15, operatorStr: '*',),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 25, numLimitBot: 25, operatorStr: '*',),
],                                                            
 [                                                            
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 5 , numLimitBot: 6 , operatorStr: ':',),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 10, numLimitBot: 10, operatorStr: ':',),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 15, numLimitBot: 15, operatorStr: ':',),
	FractionInit().createFractionScreen(gameMode: 'Practice', color: color_background_topics_1[theme], numLimitTop: 25, numLimitBot: 25, operatorStr: ':',),
]];

List<TopicPageViewItem> listFunctions = [
	TopicPageViewItem(color: color_background_topics_1[theme], subTitle: 'Learn how to manipulate the height of a function.', topic:StandartGraphMenu(clrtheme: color_background_topics_1[theme], function: (x) => x*x+3,), trainingmode: functionsPracticeList , app: functionsLevelList[0]),
	TopicPageViewItem(color: color_background_topics_2[theme], subTitle: 'Learn how to manipulate the height of a function.', topic:StandartGraphMenu(clrtheme: color_background_topics_2[theme], function: (x) => x*x+3,), trainingmode: functionsPracticeList , app: functionsLevelList[1]),
	TopicPageViewItem(color: color_background_topics_3[theme], subTitle: 'Learn how to manipulate the height of a function.', topic:StandartGraphMenu(clrtheme: color_background_topics_3[theme], function: (x) => x*x+3,), trainingmode: functionsPracticeList , app: functionsLevelList[2]),
];
List<List<FunctionsLevelCreator>>  functionsLevelList = [[
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 2, aRand: true,),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 2, bRand: true, ),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 2, aRand: true, bRand: true, ),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 2, mRand: true, ),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 2, aRand: true, bRand: true, mRand: true, ),
],[
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 5, aRand: true, exercise: "4Buttons",),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 5, bRand: true, exercise: "4Buttons",),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 5, aRand: true, bRand: true, exercise: "4Buttons",),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 5, aRand: true, bRand: true, mRand: true, exercise: "4Buttons",),
],	[
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 4, aRand: true, exercise: 'Keyboard', ),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 4, bRand: true, exercise: 'Keyboard', ),
	FunctionsLevelCreator(color: color_background_topics_1[theme], maxLevel: 4, aRand: true, bRand: true, exercise: 'Keyboard', ),
],
];
List<List<FunctionsPracticeCreator>>  functionsPracticeList = [[
	FunctionsPracticeCreator(color: color_background_topics_1[theme], aRand: true, bRand: true, mRand: true, position: 0,),
],[
	FunctionsPracticeCreator(color: color_background_topics_1[theme], aRand: true, exercise: "4Buttons", position: 0, ),
	FunctionsPracticeCreator(color: color_background_topics_1[theme], aRand: true, bRand: true, exercise: "4Buttons", position: 1, ),
	FunctionsPracticeCreator(color: color_background_topics_1[theme], aRand: true, bRand: true, mRand: true, exercise: "4Buttons", position: 2, ),
],	[
	FunctionsPracticeCreator(color: color_background_topics_1[theme], aRand: true, exercise: 'Keyboard',  position: 0, ),
	FunctionsPracticeCreator(color: color_background_topics_1[theme], bRand: true, exercise: 'Keyboard',  position: 1, ),
	FunctionsPracticeCreator(color: color_background_topics_1[theme], aRand: true, bRand: true, exercise: 'Keyboard',  position: 2, ),
],
];


List<TopicPageViewItem> listPower = [
	TopicPageViewItem(color: color_background_topics_1[theme], subTitle: 'Learn how to take numbers by the power of 2.', topic: Math.tex('1^2=?'),  app: powerLevelList[0],  trainingmode: powerPracticeList, ),
  TopicPageViewItem(color: color_background_topics_3[theme], subTitle: 'Learn how to find the root of a number.', topic: Math.tex(r'\sqrt{1}=?'),  app: powerLevelList[1], trainingmode: powerPracticeList,),
];

List<List<Widget>>  powerLevelList= [[
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 5, operatorStr: 'power', maxLevel: 3),
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 10, operatorStr: 'power', maxLevel: 6),
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 20, operatorStr: 'power', maxLevel: 5),
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 25, operatorStr: 'power', maxLevel: 5),
],
 [
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 5, operatorStr: 'root', maxLevel: 3),
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 10, operatorStr: 'root', maxLevel: 6),
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 20, operatorStr:'root', maxLevel: 5),
	PowerScreen().createPowerScreen(gameMode:'Normal', color: color_background_topics_1[theme], numLimit: 25, operatorStr: 'root', maxLevel: 5),
],
];

List<List<Widget>>  powerPracticeList= [[
	PowerScreen().createPowerScreen(gameMode:'Practice', color: color_background_topics_1[theme], numLimit: 10, operatorStr: 'power'),
	PowerScreen().createPowerScreen(gameMode:'Practice', color: color_background_topics_1[theme], numLimit: 20, operatorStr: 'power'),
	PowerScreen().createPowerScreen(gameMode:'Practice', color: color_background_topics_1[theme], numLimit: 50, operatorStr: 'power'),
],
 [
	PowerScreen().createPowerScreen(gameMode:'Practice', color: color_background_topics_1[theme], numLimit: 10, operatorStr: 'root'),
	PowerScreen().createPowerScreen(gameMode:'Practice', color: color_background_topics_1[theme], numLimit: 20, operatorStr: 'root'),
	PowerScreen().createPowerScreen(gameMode:'Practice', color: color_background_topics_1[theme], numLimit: 50, operatorStr: 'root'),
],
];

