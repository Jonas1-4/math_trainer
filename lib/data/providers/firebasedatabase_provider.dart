import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';

class FirebaseRepository{
  final DatabaseReference database =  FirebaseDatabase().reference();

  @override
  Future<Map> getGame(String gameID) async{
    Map returnMap;
    returnMap = await database.child('Games').child(gameID).once().then((value) => value.value);
    return returnMap;
  }

  @override
  Future<Map> getGames() async{
    Map returnMap;
    returnMap = await database.child('Games').once().then((value) =>  value.value);
    return returnMap;
  }

  @override
  Future<Map> getUser(String uid) async{
    Map returnMap;
    returnMap = await database.child('Users').child(uid).once().then((value) => value.value);
    return returnMap;
  }

  @override
  Future<ImageProvider> getUserPb(String uid) async{
    ImageProvider returnImage;
    returnImage = await database.child('Users').child(uid).child('photoURL').once().then((value) {
      return (value.value == null)?AssetImage('assets/anonym.png'):NetworkImage(value.value);
    });
    return returnImage;
  }
}
