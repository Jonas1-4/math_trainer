import 'package:flutter/cupertino.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:shared_preferences/shared_preferences.dart';


///Stores current Theme
int theme = SharedPrefs().getSpInt('themer');

///Sharedpreferences instance
SharedPreferences prefs;

///Current running Game
Widget currentApp;

///Current Topic
String currentTopic;

///Index of the Current Level
int currentTopicIndex = 0;

///disable Numberkeyboard while playing animations
bool buttonsdisabled = false;

//SharedPrefs Names

///Found GameIDs with uid.
String spGameIDs= 'GameIDs';

///CurrentGameID
String spCurrGameID= 'CurrGameID';

///Found Games as JSON.
String spFoundGames = 'FoundGames';

///Played games List (+GamesID).
String spPlayedGames = 'MpPlayedGames';

///Check if App is Searching Currently
String spIsSearching = 'spIsSearching';

