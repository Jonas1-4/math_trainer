import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class MultiPlayerDrawer extends StatefulWidget {
  MultiPlayerDrawer({
    this.setParentState,
    this.users,
    Key key,
  }) : super(key: key);
  Function setParentState;
  Map users;

  @override
  _MultiPlayerDrawerState createState() => _MultiPlayerDrawerState();
}
// TODO Settings Tab to change display name or profile picture delete acc etc

class _MultiPlayerDrawerState extends State<MultiPlayerDrawer> {
  DatabaseReference database = FirebaseDatabase.instance.reference();
  Map<dynamic, dynamic> usersMap = {};
  fetchData()async{
    usersMap = await database.child('Users').once().then((value) {
      return value.value;
    });
    if(mounted){
      setState(() {

      });
    }
  }
  @override
  void initState() {
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    List users = [];


          //Get all users add them to users List sort them buy score

          if (usersMap.isNotEmpty || usersMap.length >= users.length) {
            users = [];
            usersMap.forEach((key, value) {
              users.add(value);
            });
            users.sort((b, a) => (a['score'].toString()).compareTo(b['score'].toString()));
          }
          return Drawer(
            child: Container(
              color: color_background_main[theme],
              child: SafeArea(
                child: Column(children: [
                  DrawerHeader(
                    child: ColorFiltered(
                        child: Image.asset('assets/Icon.png', width: 100, height: 100),
                        colorFilter: ColorFilter.mode(color_background_main_subtitle[theme], BlendMode.srcIn)),
                    decoration: BoxDecoration(color: color_background_main[theme]),
                  ),
                  Expanded(
                    child: Container(
                      child: RefreshIndicator(
                        onRefresh: () async{
                          await fetchData();
                        },
                        child: ListView(
                            children: users
                                .map((e) => Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(16.0, 3, 8, 3),
                                          child: Row(children: [
                                            Text(
                                              (users.indexOf(e) + 1).toString() + '. ' + e['displayName'].toString(),
                                              style: TextStyle(color: color_hints[theme]),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              e['score'].toString(),
                                              style: TextStyle(color: color_hints[theme]),
                                            )
                                          ]),
                                        ),
                                        Divider()
                                      ],
                                    ))
                                .toList()),
                      ),
                    ),
                  )
                ]),
              ),
            ),
          );
  }
}
