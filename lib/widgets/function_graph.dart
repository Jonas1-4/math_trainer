
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class FunctionGraph extends StatefulWidget {
  FunctionGraph({
    Key key,
    this.minX,
    this.minY,
    this.maxX,
    this.maxY,
    this.yEnd,
    this.yStart,
    this.interval,
    this.labelColor,
    this.backgroundColor,
    this.clrtheme,
    @required this.function,
  }) : super(key: key);

  final double minX;
  final double minY;
  final double maxX;
  final double maxY;
  final double yStart;
  final double yEnd;
  final double interval;
  final Color labelColor;
  final Color backgroundColor;
  final Color clrtheme;
  Function function;

  @override
  _FunctionGraphState createState() => _FunctionGraphState();
}

class _FunctionGraphState extends State<FunctionGraph> {
  @override
  Widget build(BuildContext context) {
    Color clrtheme = widget.clrtheme ?? color_background_topics_1[theme];
    Color backgroundColor =
        widget.backgroundColor ?? color_background_main[theme];
    Color labelColor =widget.labelColor ?? color_hints[theme];
    double maxY = widget.maxY ?? 10;
    double maxX = widget.maxX ?? 10;
    double minY = widget.minY ?? -10;
    double minX = widget.minX ?? -10;
    double yStart = widget.yStart ?? -10;
    double yEnd = widget.yEnd ?? 10;
    double interval = widget.interval ?? 2;
    Function function = widget.function;
    final spots = <FlSpot>[];

    //four loop increases y (yStart) by 0.1 until it reaches yEnd
    //every loop draws a new point on the graph at (y|x(function(y)))
    for (double y = yStart; y < yEnd; y += 0.5) {
      double x = function(y);
      spots.add(new FlSpot(y, x));
    }
    return Expanded(
      child: AspectRatio(
        aspectRatio: 1/1,
          child: Center(
      child: Padding(
          padding: const EdgeInsets.fromLTRB(0,0,27,0),
          child: LineChart(LineChartData(
            clipData: FlClipData.all(),
            minX: minX,
            minY: minY,
            maxX: maxX,
            maxY: maxY,
            extraLinesData: ExtraLinesData(extraLinesOnTop: false,horizontalLines: [
              HorizontalLine(y: 0, color: labelColor, strokeWidth: 0.7),
            ], verticalLines: [
              VerticalLine(x: 0, color: labelColor, strokeWidth: 0.7),
            ]),
            gridData: FlGridData(
                drawHorizontalLine: true,
                drawVerticalLine: true,
                getDrawingHorizontalLine: (value) {
                  return FlLine(color: (labelColor), strokeWidth: 0.3);
                },
                getDrawingVerticalLine: (value) {
                  return FlLine(color: (labelColor), strokeWidth: 0.3);
                }),
            titlesData: FlTitlesData(
              bottomTitles: SideTitles(
                  showTitles: true,
                  interval: 2,
                  getTextStyles: (value) {
                    return TextStyle(color: labelColor);
                  }),
              leftTitles: SideTitles(
                  interval: 2,
                  showTitles: true,
                  getTextStyles: (value) {
                    return TextStyle(color: labelColor);
                  }),
            ),
            lineTouchData: LineTouchData(
              touchTooltipData: LineTouchTooltipData(
                  tooltipBgColor: labelColor,
                  getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
                    return touchedBarSpots.map((barSpot) {
                      final flSpot = barSpot;
                      if (flSpot.x < maxX &&
                          flSpot.x > minX &&
                          flSpot.y < maxY &&
                          flSpot.y > minY) {
                        return LineTooltipItem(
                          '( ${(flSpot.x % 1 == 0 ? flSpot.x.toInt() : flSpot.x)} | ${(flSpot.y % 1 == 0 ? flSpot.y.toInt() : flSpot.y)} )',
                          TextStyle(color: backgroundColor),
                        );
                      }
                    }).toList();
                  }),
            ),
            lineBarsData: [
              LineChartBarData(
                spots: spots,
                isCurved: true,
                barWidth: 5,
                colors: [
                  widget.clrtheme,
                ],
                dotData: FlDotData(
                  show: false,
                ),
              )
            ],
          )),
      ),
    ),
        ));
  }
}
