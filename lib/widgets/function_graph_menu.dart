

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class StandartGraphMenu extends StatefulWidget {
  StandartGraphMenu({
    Key key,
    this.minX,
    this.minY,
    this.maxX,
    this.maxY,
    this.yEnd,
    this.yStart,
    this.labelColor,
    this.backgroundColor,
    this.clrtheme,
    @required this.function,
  }) : super(key: key);

  final double minX;
  final double minY;
  final double maxX;
  final double maxY;
  final double yStart;
  final double yEnd;
  final Color labelColor;
  final Color backgroundColor;
  final Color clrtheme;
  Function function;

  @override
  _StandartGraphMenuState createState() => _StandartGraphMenuState();
}

class _StandartGraphMenuState extends State<StandartGraphMenu> {
  @override
  Widget build(BuildContext context) {
    Color clrtheme = widget.clrtheme ?? color_background_topics_1[theme];
    Color backgroundColor =
        widget.backgroundColor ?? color_background_main[theme];
    Color labelColor =widget.labelColor ?? color_hints[theme];
    double maxY = widget.maxY ?? 10;
    double maxX = widget.maxX ?? 10;
    double minY = widget.minY ?? -10;
    double minX = widget.minX ?? -10;
    double yStart = widget.yStart ?? -10;
    double yEnd = widget.yEnd ?? 10;
    Function function = widget.function;
    final spots = <FlSpot>[];

    //four loop increases y (yStart) by 0.1 until it reaches yEnd
    //every loop draws a new point on the graph at (y|x(function(y)))
    for (double y = yStart; y < yEnd; y += 0.5) {
      double x = function(y);
      spots.add(new FlSpot(y, x));
    }
    BorderSide borderside = BorderSide(color: darken(backgroundColor), width: 3, style: BorderStyle.none);
    return Expanded(
      child: Center(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0,0,0,0),
        child: LineChart(LineChartData(
            lineTouchData: LineTouchData(enabled: false),
            extraLinesData: ExtraLinesData(
extraLinesOnTop: false,
              horizontalLines: [
            HorizontalLine(y: 0, color: labelColor, strokeWidth: 1 ,),
          ], verticalLines: [
            VerticalLine(x: 0, color: labelColor, strokeWidth: 1),
          ]),
          lineBarsData: [

            LineChartBarData(
              belowBarData: BarAreaData(show: false),
              aboveBarData: BarAreaData(show: false),
              spots: spots,
              isCurved: true,isStrokeCapRound: true,
              barWidth: 5,
              colors: [
                widget.clrtheme,
              ],
              dotData: FlDotData(
                show: false,
              ),
            )
          ],
          clipData: FlClipData.all(),
          minX: minX,
          minY: minY,
          maxX: maxX,
          maxY: maxY,
          borderData: FlBorderData(show: false, border: Border(right: borderside, left: borderside, top: borderside, bottom: borderside) ),
          gridData: FlGridData(
              drawHorizontalLine: true,
              drawVerticalLine: true,
              getDrawingHorizontalLine: (value) {
                return FlLine(color: (labelColor), strokeWidth: 0.5);
              },
              getDrawingVerticalLine: (value) {
                return FlLine(color: (labelColor), strokeWidth: 0.5);
              }),
          titlesData: FlTitlesData(
            bottomTitles: SideTitles(
              showTitles: false,),
            leftTitles: SideTitles(
                showTitles: false,
          ),
          ),
        )),
      ),
    ));
  }
}
