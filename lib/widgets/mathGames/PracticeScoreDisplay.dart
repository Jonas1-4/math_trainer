import 'package:flutter/cupertino.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class PracticeScoreDisplay extends StatelessWidget {
  const PracticeScoreDisplay({
    this.score,
    Key key,
  }) : super(key: key);
  final int score;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              score.toString(),
              style: TextStyle(
                  color: color_hints[theme],
                  fontSize: ResponsiveFlutter.of(context).fontSize(5)),
            ),
            SizedBox(
              width: 50,
            )
          ],
        ),
      ],
    );
  }
}
