import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

import 'NumberButton.dart';
import 'StringButton.dart';


//example Keyboard
//[
//[7,8,9]
//[4,5,6]
//[1,2,3]
//]

class Keyboard extends StatelessWidget {
  Keyboard({
    this.hint,
    this.buttons,
    this.stringFunctions,
    this.notifyparent,
    this.gamehandler,
    this.color,
    Key key,
  }) : super(key: key);

  bool hint;
  List<List<dynamic>> buttons;
  List<Function> stringFunctions;
  Function notifyparent;
  dynamic gamehandler;
  Color color;
  int stringFunctionsInt = 0;
  List<Widget> rowButton = [];
  List<Widget> columnButtons = [];


  @override
  Widget build(BuildContext context) {
    for (dynamic i in buttons) {
      for (dynamic value in i) {
        Function strFunc = stringFunctions[stringFunctionsInt];
        rowButton.add(SizedBox(width: 5, child: Container()));
        rowButton.add((value is int)
            ? NumberButton(
                color: color,
                number: value.toString(),
                notifyparent: notifyparent,
                gamehandler: gamehandler,
              )
            : StringButton(
                string: value,
                color: color,
                notifyparent: notifyparent,
                onPressed:
                strFunc)
            );

            if(stringFunctionsInt <= stringFunctions.length  && value is String){
              stringFunctionsInt++;
            }
              rowButton.add(SizedBox(width: 5, child: Container()));
      }
      columnButtons.add(SizedBox(height: 5, child: Container()));
      columnButtons.add((rowButton.length <= 13)
          ? Row(
              mainAxisAlignment: MainAxisAlignment.start, children: rowButton)
          :
          SizedBox(
            height: 35,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                    children: rowButton
                ),
              )
            );
      columnButtons.add(SizedBox(height: 5, child: Container()));
      rowButton = [];
    }
    return SizedBox(
      width: 330,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          (hint != null)?Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Fell free to scroll the upper Row', style: TextStyle(color: color_hints[theme],), ),
              Icon(Icons.keyboard_arrow_right, color: color_hints[theme],
              )
            ],
          ): Container(),
          SizedBox(height:20),
          Column(children: columnButtons),
          SizedBox(
            height: 30,
            child: Container(),
          ),
        ],
      ),
    );
  }
}

