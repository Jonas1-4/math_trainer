
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class NumberButton extends StatelessWidget {
  NumberButton({
    this.color,
    this.number,
    this.gamehandler,
    this.notifyparent,
    Key key,
  }) : super(key: key);
  Color color;
  String number;
  dynamic gamehandler;
  Function notifyparent;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 35,
      child: FlatButton(
          //if more topics switch statement
          onPressed: () {
            if(!buttonsdisabled){
            if (currentTopic != 'Fractions') {
              gamehandler.wert == '0'
                  ? gamehandler.wert = number
                  : gamehandler.wert =
                  gamehandler.wert + number;
                  print(gamehandler.wert);
            } else if (currentTopic == 'Fractions') {
              if (!gamehandler.num2sel) {
                gamehandler.inputTop == '0'
                    ? gamehandler.inputTop = number
                    : gamehandler.inputTop = gamehandler.inputTop + number;
              } else {
                gamehandler.inputBot == '0'
                    ? gamehandler.inputBot = number
                    : gamehandler.inputBot = gamehandler.inputBot + number;
              }
          }
          notifyparent();}
          },
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18),
              side: BorderSide(color: color)),
          child: Text(
            number,
            style: TextStyle(color: color),
          )),
    );
  }
}
