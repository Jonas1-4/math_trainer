
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class StringButton extends StatelessWidget {
  StringButton({
    this.color,
    this.string,
    this.notifyparent,
    this.onPressed,
    Key key,
  }) : super(key: key);

  Color color;
  String string;
  Function notifyparent;
  Function onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 35,
      child: FlatButton(
        onPressed: () {
          if(!buttonsdisabled){
            onPressed();
            notifyparent();}
          },
          //shape: RoundedRectangleBorder(
          //    borderRadius: BorderRadius.circular(18),
          //    side: BorderSide(color: color)),
          child: Text(
            string,
            style: TextStyle(color: color),
          )),
    );
  }
}
