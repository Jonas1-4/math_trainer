import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/gamehandler_functions.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class FourButtons extends StatelessWidget {
  const FourButtons({
    Key key,
    @required this.ghf,
    @required this.color,
    @required this.onRight,
    @required this.onWrong,
  }) : super(key: key);

  final GameHandlerFunctions ghf;
  final Color color;
  final Function onRight;
  final Function onWrong;

  @override
  Widget build(BuildContext context) {
    print('2. a =' + ghf.a.toString());
    print('2. b =' + ghf.b.toString());
    print('2. m =' + ghf.m.toString());
    return Column(children: [
      Expanded(
        child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Button(1, context),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
            child: VerticalDivider(
              color: darken(color),
            ),
          ),
          Button(2, context),
        ]),
        //Expanded(child: VerticalDivider()),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Divider(
          color: (color),
        ),
      ),
      Expanded(
        child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Button(3, context),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 16),
            child: VerticalDivider(
              color: darken(color),
            ),
          ),
          Button(4, context),
        ]),
        //Expanded(child: VerticalDivider()),
      ),
    ]);
  }

  Expanded Button(
    int position,
    dynamic context,
  ) {
    return Expanded(
        child: FlatButton(
      child: FittedBox(
        fit: BoxFit.cover,
        child: DefaultTextStyle.merge(
            child: ghf.buttonList[position - 1],
            style: TextStyle(
                color: color,
                fontSize: ResponsiveFlutter.of(context).fontSize(2))),
      ),
      onPressed: (!buttonsdisabled)
          ? (position == ghf.realButton)
              ? onRight
              : onWrong
          : () => null,
    ));
  }
}
