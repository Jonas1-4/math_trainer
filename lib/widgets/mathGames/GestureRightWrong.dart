import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class GestureRightWrong extends StatelessWidget {
  GestureRightWrong({
    Key key,
    this.isCorrect,
    this.onRight,
    this.onWrong,
  }) : super(key: key);
  bool isCorrect;
  Function onRight;
  Function onWrong;

  @override
  Widget build(BuildContext context) {
    print(isCorrect);
    bool swiperight = false;
    bool swipeleft = false;
    return GestureDetector(
        onHorizontalDragUpdate: (details) {
          if (!buttonsdisabled) {
            if (details.delta.dx > 2) {
              debugPrint('User swiped right');
              swiperight = true;
              swipeleft = false;
            } else if (details.delta.dx < -2) {
              debugPrint('User swiped left');
              swipeleft = true;
              swiperight = false;
            }
          }
        },
        onHorizontalDragEnd: (details) {
          if (!buttonsdisabled) {
            if (swiperight || swipeleft) {
              if ((swiperight && isCorrect) || (swipeleft && !isCorrect)) {
                onRight();
              } else {
                onWrong();
              }
            }
          }
        },
        child: Container(
          color: color_background_main[theme],
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 3),
                    child: Icon(Icons.arrow_left, color: color_hints[theme]),
                  ),
                  Text(
                    'Swipe left if wrong',
                    style: TextStyle(color: color_hints[theme]),
                  ),
                  SizedBox(
                    width: 45,
                  )
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 45,
                  ),
                  Text(
                    'Swipe right if correct',
                    style: TextStyle(color: color_hints[theme]),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 3),
                    child: Icon(Icons.arrow_right, color: color_hints[theme]),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
