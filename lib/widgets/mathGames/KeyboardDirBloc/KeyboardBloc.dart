/*
example Keyboard
[
[7,8,9]
[4,5,6]
[1,2,3]
]
*/
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

import 'KeyboardButtonBloc.dart';

class KeyboardBloc extends StatelessWidget {
  KeyboardBloc({
    this.hint,
    this.buttons,
    this.stringFunctions,
    this.numberFunction,
    this.color,
    Key key,
  }) : super(key: key);

  final bool hint;
  final List<List<dynamic>> buttons;
  final List<Function> stringFunctions;
  final Function numberFunction;
  final Color color;

  @override
  Widget build(BuildContext context) {
    List<Widget> rowButton = [];
    List<Widget> columnButtons = [];
    int stringFunctionsInt = 0;

    for (dynamic i in buttons) {
      for (dynamic value in i) {
        Function strFunc = stringFunctions[stringFunctionsInt];
        rowButton.add(SizedBox(width: 5, child: Container()));
        rowButton.add((value is int)
            ? KeyboardButtonBloc(
                color: color,
                string: value.toString(),
                onPressed: numberFunction,
              )
            : KeyboardButtonBloc(
                string: value,
                color: color,
                onPressed: strFunc,
              ));

        if (stringFunctionsInt <= stringFunctions.length && value is String) {
          stringFunctionsInt++;
        }
        rowButton.add(SizedBox(width: 5, child: Container()));
      }
      columnButtons.add(SizedBox(height: 5, child: Container()));
      columnButtons.add((rowButton.length <= 13)
          ? Row(mainAxisAlignment: MainAxisAlignment.start, children: rowButton)
          : SizedBox(
              height: 35,
              child: ListView(
                  scrollDirection: Axis.horizontal, children: rowButton),
            ));
      columnButtons.add(SizedBox(height: 5, child: Container()));
      rowButton = [];
    }
    return SizedBox(
      width: 330,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          (hint != null)
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Feel free to scroll the upper Row',
                      style: TextStyle(
                        color: color_hints[theme],
                      ),
                    ),
                    Icon(
                      Icons.keyboard_arrow_right,
                      color: color_hints[theme],
                    )
                  ],
                )
              : Container(),
          SizedBox(height: 20),
          Column(children: columnButtons),
          SizedBox(
            height: 30,
            child: Container(),
          ),
        ],
      ),
    );
  }
}
