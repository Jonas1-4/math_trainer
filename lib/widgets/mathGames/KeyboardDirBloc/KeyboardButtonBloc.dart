import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class KeyboardButtonBloc extends StatelessWidget {
  KeyboardButtonBloc({
    this.color,
    this.string,
    this.onPressed,
    Key key,
  }) : super(key: key);

  final Color color;
  final String string;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 40,
      child: TextButton(
          onPressed: () {
            onPressed(string);
          },
          //shape: RoundedRectangleBorder(
          //    borderRadius: BorderRadius.circular(18),
          //    side: BorderSide(color: color)),
          child: Text(
            string,
            style: TextStyle(color: color, fontSize: 17),
          )),
    );
  }
}
