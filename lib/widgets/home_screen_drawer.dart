import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/multiplayer/main_screen/mp_mainscreen.dart';
import 'package:math_trainer/activities/multiplayer/sign_in.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/firebase/authenticate.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreenDrawer extends StatefulWidget {
  HomeScreenDrawer({
    this.setParentState,
    Key key,
  }) : super(key: key);
  Function setParentState;

  @override
  _HomeScreenDrawerState createState() => _HomeScreenDrawerState();
}

// TODO Settings Tab to change display name or profile picture delete acc etc

class _HomeScreenDrawerState extends State<HomeScreenDrawer> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return Drawer(
        child: Container(
      color: color_background_main[theme],
      child: SafeArea(
        child: Column(children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  child: ColorFiltered(
                      child: Image.asset('assets/Icon.png',
                          width: 100, height: 100),
                      colorFilter: ColorFilter.mode(
                          color_background_main_subtitle[theme],
                          BlendMode.srcIn)),
                  decoration:
                      BoxDecoration(color: color_background_main[theme]),
                ),
                SizedBox(
                  height: ResponsiveFlutter.of(context).scale(10),
                ),
                if (Platform.isIOS || Platform.isAndroid)
                  Column(children: [
                    TextButton(
                      onPressed: () {
                        if (FirebaseAuth.instance.currentUser != null) {
                          runApp(MpMainScreen());
                        } else {
                          runApp(SignIn());
                        }
                      },
                      child: Row(
                        children: [
                          Icon(Icons.group, color: color_hints[theme]),
                          SizedBox(
                            width: ResponsiveFlutter.of(context).scale(10),
                          ),
                          Text('Multiplayer',
                              style: TextStyle(color: color_hints[theme])),
                        ],
                      ),
                    ),
                    (FirebaseAuth.instance.currentUser == null)
                        ? TextButton(
                            onPressed: () {
                              runApp(SignIn());
                            },
                            child: Row(
                              children: [
                                Icon(Icons.login, color: color_hints[theme]),
                                SizedBox(
                                  width:
                                      ResponsiveFlutter.of(context).scale(10),
                                ),
                                Text(
                                  'Sign-In',
                                  style: TextStyle(color: color_hints[theme]),
                                ),
                              ],
                            ),
                          )
                        : TextButton(
                            onPressed: () {
                              AuthenticationService(FirebaseAuth.instance)
                                  .signOutGoogle();
                              AuthenticationService(FirebaseAuth.instance)
                                  .signOut();
                              runApp(HomeScreen());
                            },
                            child: Row(
                              children: [
                                Icon(Icons.login, color: color_hints[theme]),
                                SizedBox(
                                  width:
                                      ResponsiveFlutter.of(context).scale(10),
                                ),
                                Text(
                                  'Sign-Out',
                                  style: TextStyle(color: color_hints[theme]),
                                ),
                              ],
                            ),
                          ),
                  ])
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 0, 10.0, 20),
              child: SafeArea(
                child: Column(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            _launchPatreon();
                          },
                          child: Row(
                            children: [
                              SizedBox(
                                width: ResponsiveFlutter.of(context).scale(12),
                              ),
                              SizedBox(
                                  height:
                                      ResponsiveFlutter.of(context).scale(18),
                                  width:
                                      ResponsiveFlutter.of(context).scale(20),
                                  child: Image(
                                      image: AssetImage(
                                          'assets/PatreonLogo.png'))),
                              SizedBox(
                                width: ResponsiveFlutter.of(context).scale(10),
                              ),
                              Text(
                                'Support us',
                                style: TextStyle(
                                  color: color_hints[theme],
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Divider(
                                color: color_background_main_title[theme],
                              ),
                            ),
                            Text(
                              'Theme',
                              style: TextStyle(color: color_hints[theme]),
                            ),
                            Expanded(
                              child: Divider(
                                color: color_background_main_title[theme],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    SharedPrefs().setSpInt('themer', 0);
                                    theme = 0;
                                  });
                                  widget.setParentState();
                                },
                                child: CircleAvatar(
                                  radius: 15,
                                  backgroundColor:
                                      color_background_main_title[0],
                                  child: CircleAvatar(
                                    radius: 13,
                                    backgroundColor: color_background_main[0],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    SharedPrefs().setSpInt('themer', 1);
                                    theme = 1;
                                  });
                                  widget.setParentState();
                                },
                                child: CircleAvatar(
                                  radius: 15,
                                  backgroundColor:
                                      color_background_main_title[1],
                                  child: CircleAvatar(
                                    radius: 13,
                                    backgroundColor: color_background_main[1],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ))
        ]),
      ),
    ));
  }

  _launchPatreon() async {
    const url = 'https://www.patreon.com/fractalapps';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
