import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class Progressbar extends StatelessWidget {
  Progressbar({
      Key key,
      this.color,
      this.value,
      this.padding,
      
  }) : super(key: key);
  
      Color color;
      double value;
      double padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding??0),
      child: LinearProgressIndicator(
        backgroundColor: color_hints[theme],
        valueColor: new AlwaysStoppedAnimation(color),
        value: value,
      ),
    );
  }
}
