import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/topic_select.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/shared_prefs.dart';

class MainMenuCard extends StatelessWidget {
  MainMenuCard({
    this.maxLevel,
    this.levelList,
    this.subTitle,
    this.title,
    Key key,
  }) : super(key: key);
  final String title;
  final String subTitle;
  final int maxLevel;
  final List levelList;

  @override
  Widget build(BuildContext context) {
    double percent = SharedPrefs().getSpInt(title + 'percentage') /
        maxLevel *
        100;
    String percentStr = (percent % 1 != 0) ?
    percent.toStringAsFixed(2) : percent.toString();
    return Padding(
      padding: const EdgeInsets.fromLTRB(18, 0, 18, 12),
      child: Center(
          child: InkWell(
            onTap: (){
              currentTopic = title;
              Navigator.push(context, new MaterialPageRoute(builder: (context) => new TopicSelect(items: levelList)));
            },
            child: Card(
                color: color_background_main[theme],
                elevation: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 12, 0, 0),
                      child: Text(title,
                          style: TextStyle(
                              color: color_background_main_title[theme],
                              fontSize: 20)),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                      child: Text(subTitle,
                          style: TextStyle(
                            color: color_background_main_subtitle[theme],
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                                  percentStr +
                                  "%",
                              style: TextStyle(
                                  color: color_background_main_subtitle[theme])),
                        ),
                      ],
                    )
                  ],
                )),
          )),
    );
  }
}
