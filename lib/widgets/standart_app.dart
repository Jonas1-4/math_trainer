
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class StandartApp extends StatelessWidget {
  StandartApp({
    Key key,
    this.child,
    this.color,
    this.onWillPop,
  }) : super(key: key);

  Color color;
  Widget child;
  Function onWillPop;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: WillPopScope(
        child: Scaffold(
          backgroundColor: color_background_main[theme],
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.close, color: color),
                  onPressed: onWillPop,)
            ],
          ),
          body: SafeArea(
            child: child,
          ),
        ),
        onWillPop: (!buttonsdisabled)?onWillPop:null,
      ),
    );
  }
}
