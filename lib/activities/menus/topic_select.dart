import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/widgets/progressbar.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'home_screen.dart';

int _level = 0;

class TopicSelect extends StatefulWidget {
  const TopicSelect({
    Key key,
    this.items,
  }) : super(key: key);
  final List<TopicPageViewItem> items;
  @override
  _TopicSelectState createState() => _TopicSelectState();
}

class _TopicSelectState extends State<TopicSelect> {
  PageController pageController;
  Animatable<Color> background;

  @override
  void initState() {
    _initialize();
    super.initState();
  }

  void _initialize() {
    background = TweenSequence<Color>(widget.items.map((e) {
      Color color1 = e.color;
      Color color2;
      if (widget.items.length == widget.items.indexOf(e) + 1) {
        color2 = e.color;
      } else {
        color2 = widget.items[widget.items.indexOf(e) + 1].color;
      }
      return TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(begin: color1, end: color2),
      );
    }).toList());
    pageController = PageController();
  }

  @override
  void reassemble() {
    pageController.dispose();
    _initialize();
    super.reassemble();
  }

  @override
  Widget build(BuildContext context) {
    return StandartApp(
      color: color_hints[theme],
      onWillPop: () {
        Navigator.pop(context, true);
      },
      child: AnimatedBuilder(
        animation: pageController,
        builder: (context, child) {
          return DecoratedBox(
            decoration: BoxDecoration(
              //color: background.evaluate(AlwaysStoppedAnimation(color)),
              color: color_background_main[theme],
            ),
            child: child,
          );
        },
        child: PageView.builder(
            controller: pageController,
            itemCount: widget.items.length,
            itemBuilder: (context, position) {
              _level = SharedPrefs()
                  .getSpInt(currentTopic + position.toString() + 'Level');
              currentTopicIndex = position;
              print('IndexCurrTopic: $currentTopicIndex');
              widget.items[position].position = position;
              return widget.items[position];
            },
            onPageChanged: (int page) {}),
      ),
    );
  }
}

class TopicPageViewItem extends StatelessWidget {
  TopicPageViewItem({
    this.color,
    this.app,
    this.operatorStr,
    this.subTitle,
    this.topic,
    this.trainingmode,
    Key key,
  }) : super(key: key);

  final Color color;
  final String subTitle;
  final String operatorStr;
  final List<Widget> app;
  final List<List<Widget>> trainingmode;
  final Widget topic;
  int position;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          if (_level >= app.length) {
            currentApp = LevelLayout(trainingmode: trainingmode);
          } else {
            currentApp = app[_level];
          }
          Navigator.push(context, new MaterialPageRoute(builder: (context) => currentApp));
        },
        child: SizedBox.expand(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 300,
                      width: 270,
                      child: InkWell(
                        onTap: () {
                          currentTopicIndex = position;
                          if (_level >= app.length) {
                            currentApp = LevelLayout(
                              trainingmode: trainingmode,
                            );
                          } else {
                            currentApp = app[_level];
                          }
                          runApp(currentApp);
                        },
                        child: Card(
                          elevation: 5,
                          color: (color_background_main[theme]),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: SizedBox.expand(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                topic is Math
                                    ? DefaultTextStyle.merge(
                                        //made the if statement so i dont have that long levellists.
                                        style: (TextStyle(
                                            color: color,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(3))),
                                        child: topic,
                                      )
                                    : topic,
                                SizedBox(height: 10),
                                Progressbar(
                                  color: color,
                                  value: _level / app.length,
                                  padding: 8,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(subTitle,
                                      style: TextStyle(color: brighten(color))),
                                ),
                              ],
                            )),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 80),

                    // TODO add explanations for each Topic

                    //TextButton(
                    //  style: TextButton.styleFrom(
                    //    shape: RoundedRectangleBorder(
                    //        borderRadius: BorderRadius.circular(15),
                    //        side: BorderSide(
                    //            color: color_background_main[theme])),
                    //    primary: color,
                    //    backgroundColor: color,
                    //  ),
                    //  onPressed: () => null,
                    //  child: Text('Explanation',
                    //      style:
                    //          TextStyle(color: color_background_main[theme])),
                    //)
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class LevelLayout extends StatelessWidget {
  const LevelLayout({
    Key key,
    @required this.trainingmode,
  }) : super(key: key);

  final List<List<Widget>> trainingmode;

  @override
  Widget build(BuildContext context) {
    return StandartApp(
      onWillPop: () {
        runApp(HomeScreen());
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: SizedBox(
              height: 300,
              width: 270,
              child: Card(
                elevation: 5,
                color: (color_background_main[theme]),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: trainingmode[currentTopicIndex].map((e) {
                            int levelPos =
                                trainingmode[currentTopicIndex].indexOf(e);
                            return Column(
                              children: [
                                InkWell(
                                  onTap: () {
                                    runApp(e);
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                          'Level ' +
                                              (levelPos + 1).toString() +
                                              ":",
                                          style: TextStyle(
                                              color:
                                                  color_background_main_title[
                                                      theme])),
                                      Expanded(child: Container()),
                                      Text(
                                          SharedPrefs()
                                              .getSpInt(currentTopic +
                                                  currentTopicIndex.toString() +
                                                  'PracticeScore' +
                                                  levelPos.toString())
                                              .toString(),
                                          style: TextStyle(
                                              color:
                                                  color_background_main_title[
                                                      theme]))
                                    ],
                                  ),
                                ),
                                Divider(color: color_hints[theme])
                              ],
                            );
                          }).toList() ??
                          [Container()]),
                ),
              ),
            ),
          ),
          SizedBox(height: 80),
          TextButton(
            style: TextButton.styleFrom(
              primary: Colors.transparent,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(color: color_background_main[theme])),
            ),
            onPressed: () => null,
            child: Text('Explanation',
                style: TextStyle(color: Colors.transparent)),
          )
        ],
      ),
    );
  }
}
