import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/multiplayer/main_screen/mp_mainscreen.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/widgets/standart_app.dart';

class AfterMultiplayerScreen extends StatelessWidget {
  AfterMultiplayerScreen({this.color, this.ownScore, this.oppPb, this.oppScore, this.ownPb});

  Color color;
  List ownScore, oppScore;
  ImageProvider ownPb, oppPb;

  @override
  Widget build(BuildContext context) {
    //better way?

    return StandartApp(
      color: color_background_main[theme],
      onWillPop: () {
        SharedPrefs().setSpStrList(spGameIDs, []);
        runApp(MpMainScreen());
      },
      child: Builder(
        builder: (context) {
          return Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Center(
                child: Padding(padding: const EdgeInsets.all(16.0), child: Center()),
              ),
              Row(
                children: [
                  SizedBox(
                    width: 50,
                  ),
                  CircleAvatar(
                    backgroundColor: color_hints[theme],
                    backgroundImage: oppPb,
                  ),
                  Expanded(child: Container()),
                  CircleAvatar(
                    backgroundColor: color_hints[theme],
                    backgroundImage: ownPb,
                  ),
                  SizedBox(
                    width: 50,
                  ),
                ],
              ),
              for (int i = 0; i <= oppScore.length - 1; i++)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      SizedBox(width: 50),
                      CircleAvatar(
                        radius: 10,
                        backgroundColor: darken(color_background_main[theme], 30),
                        child: CircleAvatar(
                            radius: 8, backgroundColor: (oppScore[i] == 0) ? color_wrong[theme] : color_right[theme]),
                      ),
                      Expanded(child: Container()),
                      CircleAvatar(
                        radius: 10,
                        backgroundColor: darken(color_background_main[theme], 30),
                        child: CircleAvatar(
                            radius: 8, backgroundColor: (ownScore[i] == '0') ? color_wrong[theme] : color_right[theme]),
                      ),
                      SizedBox(width: 50),
                    ],
                  ),
                ),
              Expanded(
                child: Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  IconButton(
                    onPressed: () {
                      runApp(HomeScreen());
                    },
                    icon: Icon(
                      Icons.home,
                      color: color_background_main_subtitle[theme],
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  InkWell(
                    onTap: () {
                      SharedPrefs().setSpStrList(spGameIDs, []);
                      runApp(MpMainScreen());
                    },
                    child: Row(
                      children: [
                        Text(
                          'Continue',
                          style: TextStyle(color: color_background_main_subtitle[theme]),
                        ),
                        IconButton(
                          onPressed: () {
                            SharedPrefs().setSpStrList(spGameIDs, []);
                            runApp(MpMainScreen());
                          },
                          icon: Icon(Icons.arrow_forward_sharp),
                          color: color_background_main[theme],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              )
            ],
          );
        },
      ),
    );
  }
}
