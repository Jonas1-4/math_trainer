import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_level.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/widgets/home_screen_drawer.dart';
import 'package:math_trainer/widgets/home_screen_menu_card.dart';

import 'topic_select.dart';

BuildContext _context;

//Has to be Statefull because of theme change
class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    _context = context;
    final _scaffoldKey = GlobalKey<ScaffoldState>();

    int maxLevel(List levelList) {
      int maxLevel = 0;
      for (int i = 0; i <= levelList.length - 1; i++) {
        maxLevel += levelList[i].length;
      }
      return maxLevel;
    }

    List<MainMenuCard> menuCardList = [
      MainMenuCard(
          title: 'Basic',
          subTitle: 'Learn addition, multiplication and other basics',
          levelList: listBasic,
          maxLevel: maxLevel(fractionsLevelList)),
      MainMenuCard(
          title: 'Fractions',
          subTitle: 'Learn the basics of calculation with fractions',
          levelList: listFractions,
          maxLevel: maxLevel(fractionsLevelList)),
      MainMenuCard(
          title: 'Power',
          subTitle: 'Learn the basics of calculating with powers and roots',
          levelList: listPower,
          maxLevel: maxLevel(powerLevelList)),
      MainMenuCard(
          title: 'Basic function-graphs',
          subTitle:
              'Learn the basics of displaying and reading Function on a graph',
          levelList: listFunctions,
          maxLevel: maxLevel(functionsLevelList))
    ];

    // TODO levels only clickable if min 25% in previous levels??

    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return MaterialApp(
      home: WillPopScope(
          child: Scaffold(
              key: _scaffoldKey,
              appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.transparent,
                elevation: 0,
                actions: <Widget>[
                  InkWell(
                    onTap: () => _scaffoldKey.currentState.openDrawer(),
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () => null,
                          icon: Icon(
                            Icons.group,
                            color: color_background_main_title[theme],
                          ),
                          color: color_background_main_subtitle[0],
                        ),
                        Text(
                          'Multiplayer ',
                          style: TextStyle(
                              color: color_background_main_title[theme]),
                        ),
                        SizedBox(
                          width: 10,
                        )
                      ],
                    ),
                  )
                ],
              ),
              drawer: HomeScreenDrawer(
                setParentState: refresh,
              ),
              backgroundColor: color_background_main[theme],
//
//Screen Part
//

              body: Builder(builder: (BuildContext context) {
                _context = context;
                return SafeArea(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(19, 10, 0, 0),
                        child: Text(
                          'Math Trainer',
                          style: TextStyle(
                              color: color_background_main_title[theme],
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 0, 30),
                        child: Text(
                          'Learn math effective and fast',
                          style: TextStyle(
                              fontFamily: 'Schyler',
                              color: color_background_main_subtitle[theme]),
                        ),
                      ),

//
// Menu Card Part
//
                      Expanded(
                        child: ListView(children: menuCardList),
                      ),
                      Padding(
                        padding: EdgeInsets.all(16),
                        child: Center(
                          child: Text(
                            'JESUS DIED FOR US!',
                            style: TextStyle(
                                color: color_background_main_subtitle[theme],
                                fontSize: 14),
                          ),
                        ),
                      )
                    ]));
              })),
//
// TODO When back is pressed it should display dialog asking if sure to close
//
          onWillPop: () => showDialog<bool>(
              context: _context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Warning'),
                  content: Text('Do you really want to exit'),
                  actions: [
                    TextButton(
                      child: Text('Yes'),
                      onPressed: () => Navigator.pop(context, true),
                    ),
                    TextButton(
                      child: Text('No'),
                      onPressed: () => Navigator.pop(context, false),
                    ),
                  ],
                );
              })),
    );
  }

  refresh() {
    if (mounted) {
      setState(() {});
    }
  }
}
