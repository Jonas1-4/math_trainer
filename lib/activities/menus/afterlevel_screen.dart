import 'dart:math';

import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class AfterLevelScreen extends StatelessWidget {
  AfterLevelScreen({
    this.levelList,
  });
  final List<List<Widget>> levelList;


  @override
  Widget build(BuildContext context) {
    
  int  currentLevel = SharedPrefs().getSpInt(currentTopic + currentTopicIndex.toString() + 'Level'), maxLevel =  levelList[currentTopicIndex].length;
  Widget nextLevel = currentLevel >= maxLevel ? HomeScreen() : levelList[currentTopicIndex][currentLevel];

    List<String> quotes = [
      'You are awesome',
      'Congratulation!',
      'You are a masterpiece',
      'Nice!',
      'Perfect!',
      'If I were you, I would be pretty proud',
      "I'm straight up stunned",
      'You are a genius mate',
      'You did well, Son',
      'You is smart. \n You is important.',
      'You are killin it, dude',
      "Don't spent all \n the dopamine \n at once!",
      'You really have beautiful eyeballs',
    ];
    Random random = new Random();
    int randomInt = random.nextInt(quotes.length);
    String quote = quotes[randomInt];
    buttonsdisabled = false;
    return StandartApp(
      color: color_background_main[theme],
      onWillPop: () {
        runApp(HomeScreen());
      },
      child: Builder(
        builder: (context) {
          return Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Center(
                    child: Text(quote,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: color_background_main_title[theme],
                          fontSize: ResponsiveFlutter.of(context).fontSize(5),
                        )),
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Center(
                child: SizedBox(
                  height: 150,
                  width: 150,
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          height: 150,
                          width: 150,
                          child: CircularProgressIndicator(
                            backgroundColor:
                                darken(color_background_main[theme]),
                            value: currentLevel / maxLevel,
                            valueColor: AlwaysStoppedAnimation(
                                color_background_main_title[theme]),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          currentLevel.toString() + ' / ' + maxLevel.toString(),
                          style: TextStyle(
                              color: color_background_main_subtitle[theme],
                              fontSize: 40),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  IconButton(
                    onPressed: () {
                      runApp(HomeScreen());
                    },
                    icon: Icon(
                      Icons.home,
                      color: color_background_main_subtitle[theme],
                    ),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  InkWell(
                    onTap: () {
                      runApp(nextLevel);
                    },
                    child: Row(
                      children: [
                        Text(
                          'Continue',
                          style: TextStyle(
                              color: color_background_main_subtitle[theme]),
                        ),
                        IconButton(
                            onPressed:() =>null,
                          icon: Icon(Icons.arrow_forward_sharp),
                          color: color_background_main[theme],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              )
            ],
          );
        },
      ),
    );
  }
}
