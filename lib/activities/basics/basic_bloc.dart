import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/data/keyboard_level_model.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/functions/functions_strings.dart';
import 'package:meta/meta.dart';

part 'basic_event.dart';
part 'basic_state.dart';

class BasicBloc extends Bloc<BasicEvent, BasicState> {
  BasicBloc(BasicState initialState) : super(initialState);
  KeyboardLevelModel basicModel;
  @override
  Stream<BasicState> mapEventToState(
    BasicEvent event,
  ) async* {
    if (event is InitiateBasic) {
      print('event.maxlevel: ${event.maxLevel}');
      basicModel ??= new KeyboardLevelModel(
          operatorStr: event.operatorStr,
          maxLevel: event.maxLevel,
          numLimit: event.numLimit,
          minus: event.minus ?? false);
      basicModel.reset();

      Random random = new Random();
      int num1, num2;
      //switch statement is needed for number generation
      num1 = random.nextInt(event.numLimit) + 1;
      num2 = random.nextInt(event.numLimit) + 1;
      if (basicModel.minus) {
        num1 = num1 - (event.numLimit / 2).round();
        num2 = num2 - (event.numLimit / 2).round();
      }
      if (event.operatorStr == '/') {
        basicModel.exerciseNumbers.add(num1 * num2);
        basicModel.exerciseNumbers.add(num2);
      } else {
        basicModel.exerciseNumbers.add(num1);
        basicModel.exerciseNumbers.add(num2);
      }
      switch (event.operatorStr) {
        case '+':
          {
            basicModel.solution =
                basicModel.exerciseNumbers[0] + basicModel.exerciseNumbers[1];
          }
          break;
        case '-':
          {
            basicModel.solution =
                basicModel.exerciseNumbers[0] - basicModel.exerciseNumbers[1];
          }
          break;
        case '*':
          {
            basicModel.solution =
                basicModel.exerciseNumbers[0] * basicModel.exerciseNumbers[1];
          }
          break;
        case '/':
          {
            basicModel.solution =
                (basicModel.exerciseNumbers[0] / basicModel.exerciseNumbers[1])
                    .round();
          }
          break;
      }
      basicModel.exercise =
          '\\sf{${basicModel.exerciseNumbers[0]}${basicModel.operatorStr}${basicModel.exerciseNumbers[1]} =}';
      print(basicModel.exercise);
      print(
          'numlimit: ${event.numLimit} operatorStr: ${event.operatorStr} solution: ${basicModel.solution}');
      yield ShowExercise(exercise: basicModel.exercise);
    }

    if (event is ChangeBasicValue) {
      print('score: ${basicModel.score} maxlevel: ${basicModel.maxLevel}');
      if (basicModel.input == '0') {
        basicModel.input = event.numberPressed;
      } else {
        basicModel.input = basicModel.input + event.numberPressed;
      }
      yield ShowBasicUserInput(userInput: basicModel.input);
    }

    if (event is DeleteBasicValue) {
      basicModel.input = deleteChar(basicModel.input);
      yield ShowBasicUserInput(userInput: basicModel.input);
    }

    if (event is SubmitBasicValue) {
      String correctAnswer = basicModel.solution.toString();
      Color colorInput = color_wrong[theme];
      bool maxScore = false;
      print('score: ${basicModel.score} maxlevel: ${basicModel.maxLevel}');
      if (basicModel.solution.toString() == basicModel.input) {
        basicModel.score++;
        colorInput = color_right[theme];
        if (basicModel.score >= basicModel.maxLevel) {
          maxScore = true;
          SharedPrefs().increaseSpInt(currentTopic + 'percentage', 1);
        }
      } else {
        basicModel.score--;
      }
      yield ShowAnswer(
          correctAnswer: correctAnswer,
          colorInput: colorInput,
          maxScore: maxScore);
    }

    if (event is GenerateBasicExercise) {
      yield BasicInitial();
    }
  }
}
