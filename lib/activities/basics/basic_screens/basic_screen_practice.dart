import 'package:flutter/cupertino.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:math_trainer/widgets/mathGames/KeyboardDirBloc/KeyboardBloc.dart';
import 'package:math_trainer/widgets/mathGames/PracticeScoreDisplay.dart';

import '../basic_init.dart';

class BasicScreenPractice extends StatelessWidget{
  BasicScreenPractice({
    this.color,
    this.colorInput,
    this.score,
    this.maxLevel,
    this.userInput,
    this.exercise,
  });
  
    final Color color, colorInput;
    final int score,maxLevel;
    final String exercise, userInput;

  @override 
  Widget build(BuildContext context){
    
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
         PracticeScoreDisplay(score: score),
        SizedBox(
          height: 100,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DefaultTextStyle.merge(
              child: Math.tex(r'' + exercise, mathStyle: MathStyle.display),
              style: TextStyle(
                fontSize: 40,
                color: color,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            DefaultTextStyle.merge(
              child: Math.tex(r'\sf { ' + userInput + '}'),
              style: TextStyle(
                fontSize: 40,
                color: (colorInput),
              ),
            ),
          ],
        ),
        Expanded(
          child: Container(),
        ),
        KeyboardBloc(
          numberFunction: BasicInit().changeValue,
          buttons: [
            [7, 8, 9],
            [4, 5, 6],
            [1, 2, 3],
            ['Del', 0, 'Ent']
          ],
          color: color,
          stringFunctions: [
            BasicInit().deleteChar,
            BasicInit().submit,
          ],
        ),
      ],
    );
  }
}
