part of 'basic_bloc.dart';

@immutable
abstract class BasicEvent extends Equatable {
  @override
  List<Object> get props => null;
}

class InitiateBasic extends BasicEvent {
  final String operatorStr;
  final int numLimit;
  final int maxLevel;
  final bool minus;
  InitiateBasic({
    this.operatorStr,
    this.numLimit,
    this.maxLevel,
    this.minus
  });
}

class ChangeBasicValue extends BasicEvent {
  ChangeBasicValue(
    this.numberPressed,
  );
  final String numberPressed;
}

class DeleteBasicValue extends BasicEvent {}

class GenerateBasicExercise extends BasicEvent {}

class SubmitBasicValue extends BasicEvent {
  final String userInput;
  SubmitBasicValue({
    this.userInput,
 });
}

