import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/menus/topic_select.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_level.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/functions_games_bloc.dart';
import 'package:math_trainer/widgets/standart_app.dart';

import 'basic_bloc.dart';
import 'basic_screens/basic_screen_multiplayer.dart';
import 'basic_screens/basic_screen_normal.dart';
import 'basic_screens/basic_screen_practice.dart';

class BasicInit extends StatelessWidget {
  BasicInit({Key key}) : super(key: key);

  Widget createBasicScreen(
          {Color color,
          int maxLevel,
          int numLimit,
          int num1,
          int num2,
          bool minus,
          String operatorStr,
          String gameMode}) =>
      BlocProvider<BasicBloc>(
          create: (context) => BasicBloc(BasicInitial(
              color: color,
              maxLevel: maxLevel,
              numLimit: numLimit,
              minus: minus,
              operatorStr: operatorStr,
              gameMode: gameMode)),
          child: BasicInit());

  BasicBloc _basicBloc;

  @override
  Widget build(BuildContext context) {
    _basicBloc = BlocProvider.of<BasicBloc>(context);

    return StandartApp(
      onWillPop: () {
        runApp(HomeScreen());
      },
      child: BlocBuilder<BasicBloc, BasicState>(builder: (context, state) {
        return buildBasicScreen(state);
      }),
    );
  }

  int maxLevel, score = 0, numLimit;
  Color color, colorInput;
  String exercise = '', userInput, operatorStr, gameMode;
  bool minus;

  Widget buildBasicScreen(BasicState state) {
    print(state);

    if (state is BasicInitial) {
      minus??= state.minus;
      gameMode ??= state.gameMode;
      maxLevel ??= state.maxLevel;
      color ??= state.color;
      colorInput = color;
      numLimit ??= state.numLimit;
      operatorStr ??= state.operatorStr;
      userInput = '0';
      initiateBasic(
          numLimit: numLimit, operatorString: operatorStr, maxLevel: maxLevel, minus: minus);
    }

    if (state is ShowExercise) {
      exercise = state.exercise;
    }

    if (state is ShowBasicUserInput) {
      userInput = state.userInput;
    }

    if (state is ShowAnswer) {
      userInput = state.correctAnswer;
      colorInput = state.colorInput;
      newExercise(
          numLimit: numLimit,
          operatorString: operatorStr,
          colorInput: colorInput,
          maxScore: state.maxScore);
    }

    switch (gameMode){
      case "Normal":
        return BasicScreenNormal(color: color, colorInput: colorInput, exercise: exercise, maxLevel: maxLevel, score: score);
        break;
      case "Practice":
        return BasicScreenPractice(color: color, colorInput: colorInput, exercise: exercise, maxLevel: maxLevel, score: score);
        break;
      case "Multiplayer":
        return BasicScreenMultiplayer(color: color, colorInput: colorInput, exercise: exercise, maxSeconds: maxLevel, currentSeconds: score);
        break;
      default:
        return BasicScreenNormal(color: color, colorInput: colorInput, exercise: exercise, maxLevel: maxLevel, score: score);
        break;
  }}
  void initiateBasic(
      {int numLimit, String operatorString, int maxLevel, bool minus}) {
    _basicBloc.add(InitiateBasic(
        minus: minus,
        numLimit: numLimit,
        operatorStr: operatorString,
        maxLevel: maxLevel));
  }

  void deleteChar(String value) {
    _basicBloc.add(DeleteBasicValue());
  }

  void changeValue(String number) {
    _basicBloc.add(ChangeBasicValue(number));
  }

  void submit(String value) {
    _basicBloc.add(SubmitBasicValue());
  }

  void newExercise(
      {
        int numLimit,
      String operatorString,
      Color colorInput,
      bool maxScore}) async {
    if (gameMode == 'Normal') {
      if (colorInput == color_right[theme]) {
        score++;
      } else {
        score--;
      }
      if (maxScore) {
        GameFunctions().nextGameLevels(basicsLevelList);
      }
    } else if (gameMode == 'Practice') {
      if (colorInput == color_right[theme]) {
        score++;
      } else {
        runApp(LevelLayout(trainingmode: basicsLevelList));
      }
    }

    await Future.delayed(Duration(seconds: 3));
    _basicBloc.add(GenerateBasicExercise());
    return;
  }
}

