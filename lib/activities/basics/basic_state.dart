part of 'basic_bloc.dart';

abstract class BasicState extends Equatable {
  @override
  List<Object> get props => [];
}

class BasicInitial extends BasicState {
  final int maxLevel, numLimit;
  final String operatorStr, gameMode;
  final Color color;
  final bool minus;

  BasicInitial(
     {this.maxLevel,
      this.numLimit,
      this.color,
      this.operatorStr,
      this.gameMode,
      this.minus});
}

class ShowExercise extends BasicState {
  @override
  List<Object> get props => [exercise];

  final String exercise;

  ShowExercise({
    this.exercise,
  });
}

class ShowBasicUserInput extends BasicState {
  @override
  List<Object> get props => [userInput];

  final String userInput;

  ShowBasicUserInput({
    this.userInput,
  });
}

class ShowAnswer extends BasicState {
  final Color colorInput;
  final String correctAnswer;
  final bool maxScore;

  ShowAnswer({this.correctAnswer, this.colorInput, this.maxScore});
}

class InitiateNewExercise extends BasicState {
  final int numLimit;
  final String operatorStr;

  InitiateNewExercise({this.operatorStr, this.numLimit});
}
