import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:math_trainer/activities/menus/afterlevel_screen.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_level.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/gamehandler_functions.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/functions/functions_strings.dart';
import 'package:math_trainer/widgets/function_graph.dart';
import 'package:math_trainer/widgets/progressbar.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:math_trainer/widgets/mathGames/FourButtons.dart';
import 'package:math_trainer/widgets/mathGames/GestureRightWrong.dart';
import 'package:math_trainer/widgets/mathGames/KeyboardDir/Keyboard.dart';

class FunctionsLevelCreator extends StatefulWidget {
  FunctionsLevelCreator({
    this.color,
    this.function,
    this.maxLevel,
    this.numlimit,
    this.score,
    this.aRand,
    this.bRand,
    this.mRand,
    this.exercise,
    Key key,
  }) : super(key: key);

  final Color color;
  final Function function;
  final int maxLevel, numlimit, score;
  final bool aRand, bRand, mRand;

  /// exercises: Gesture; 4Buttons
  final String exercise;

  @override
  _FunctionsLevelCreatorState createState() => _FunctionsLevelCreatorState();
}

class _FunctionsLevelCreatorState extends State<FunctionsLevelCreator> {
  GameHandlerFunctions ghf;

  @override
  void initState() {
    ghf = new GameHandlerFunctions();
    bool aRand = widget.aRand ?? false;
    bool bRand = widget.bRand ?? false;
    bool mRand = widget.mRand ?? false;

    ghf.aB = aRand;
    ghf.bB = bRand;
    ghf.mB = mRand;
    currentApp =
        FunctionsLevelCreator(color: widget.color, maxLevel: widget.maxLevel);
    ghf.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool aRand = widget.aRand ?? false;
    bool bRand = widget.bRand ?? false;
    bool mRand = widget.mRand ?? false;

    Color color = widget.color ?? color_background_topics_1[theme];
    int score = widget.score ?? 0, maxLevel = widget.maxLevel ?? 7;
    int topiclvlint;
    String exercise = widget.exercise ?? 'Gesture';

    switch (exercise) {
      case 'Gesture':
        topiclvlint = 0;
        break;
      case '4Buttons':
        topiclvlint = 0;
        break;
      case 'Keyboard':
        topiclvlint = 0;
        break;
      default:
        topiclvlint = 0;
    }

    if (!buttonsdisabled && exercise != 'Keyboard') {
      ghf = new GameHandlerFunctions();
      ghf.aB = aRand;
      ghf.bB = bRand;
      ghf.mB = mRand;
      ghf.init();
      ghf.initFourButton();
      print('a =' +
          ghf.a.toString() +
          '\n' +
          'b =' +
          ghf.b.toString() +
          '\n' +
          'm =' +
          ghf.m.toString());
    }

    void onWrong() {
      score > 0 ? score-- : null;
      buttonsdisabled = true;
      if (mounted) {
        runApp(FunctionsLevelCreator(
          color: color_wrong[theme],
          maxLevel: maxLevel,
          score: score,
          exercise: exercise,
        ));
      }
      Future.delayed(const Duration(seconds: 2), () {
        ghf.init();
        buttonsdisabled = false;
        if (mounted) {
          runApp(FunctionsLevelCreator(
            maxLevel: maxLevel,
            color: color,
            score: score,
            aRand: aRand,
            bRand: bRand,
            mRand: mRand,
            exercise: exercise,
          ));
        }
      });
    }

    void onRight() {
      score++;
      buttonsdisabled = true;
      runApp(FunctionsLevelCreator(
        score: score,
        color: color_right[theme],
        maxLevel: maxLevel,
        exercise: exercise,
      ));
      Future.delayed(const Duration(seconds: 2), () {
        ghf.init();
        buttonsdisabled = false;
        if (score < maxLevel) {
          if(mounted){
            runApp(FunctionsLevelCreator(
              score: score,
              color: color,
              maxLevel: maxLevel,
              aRand: aRand,
              bRand: bRand,
              mRand: mRand,
              exercise: exercise,
            ));
          }
        } else {
          if (SharedPrefs().getSpInt(currentTopic + currentTopicIndex.toString() + 'Level') <=
              functionsLevelList[currentTopicIndex].length) {
            SharedPrefs().increaseSpInt(currentTopic + 'percentage', 1);
            SharedPrefs().increaseSpInt(currentTopic + currentTopicIndex.toString() + 'Level', 1);
          }
          runApp(AfterLevelScreen(
                  levelList: functionsLevelList
          ));
        }
      });
    }

    void refresh() {
      setState(() {});
    }

    print(ghf.wert);

    return StandartApp(
        onWillPop: () {
          runApp(HomeScreen());
        },
        color: widget.color,
        child: Builder(builder: (context) {
          return Column(mainAxisAlignment: MainAxisAlignment.end, children: [
            Column(
              children: [
                Progressbar(
                    color: color,
                    value: (score / maxLevel).toDouble(),
                    padding: 8),
                SizedBox(
                  height: 50,
                  child: 
                  (exercise == 'Gesture' || exercise == 'Keyboard') ?
                       Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            DefaultTextStyle.merge(
                                child:
                                    (exercise == 'Gesture' || buttonsdisabled)
                                        ? Math.tex(r'\rm{f(x)=' +
                                            ghf.mStr(ghf.exerciseiscorrect) +
                                            ghf.aStr(ghf.exerciseiscorrect) +
                                            ghf.bStr(ghf.exerciseiscorrect) +
                                            '}')
                                        : Math.tex(r'\rm{f(x)=' + ghf.wert + '}'),
                                style: TextStyle(color: color, fontSize: 25)),
                          ],
                        )
                :Container())
              ],
            ),
            FunctionGraph(
              function: (x) => ghf.m * pow(x + ghf.a, 2) + ghf.b,
              clrtheme: color,
            ),
            Expanded(
                child: (exercise == 'Gesture')
                    ? GestureRightWrong(
                        isCorrect: ghf.exerciseiscorrect,
                        onRight: onRight,
                        onWrong: onWrong,
                      )
                    : (exercise == '4Buttons')
                        ? FourButtons(
                            ghf: ghf,
                            color: color,
                            onRight: onRight,
                            onWrong: onWrong,
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Keyboard(
                                hint: true,
                                color: color,
                                gamehandler: ghf,
                                buttons: [
                                  ['+', '-', '(', ')', 'x', '^'],
                                  [7, 8, 9],
                                  [4, 5, 6],
                                  [1, 2, 3],
                                  ['Del', 0, 'Ent'],
                                ],
                                stringFunctions: [
                                  // TODO make method which takes the str and put it there but without calling it
                                  () {
                                    if (ghf.wert != '0') {
                                      ghf.wert = ghf.wert + '+';
                                    } else {
                                      ghf.wert = '+';
                                    }
                                  },
                                  () {
                                    if (ghf.wert != '0') {
                                      ghf.wert = ghf.wert + '-';
                                    } else {
                                      ghf.wert = '-';
                                    }
                                  },
                                  () {
                                    if (ghf.wert != '0') {
                                      ghf.wert = ghf.wert + '(';
                                    } else {
                                      ghf.wert = '(';
                                    }
                                  },
                                  () {
                                    if (ghf.wert != '0') {
                                      ghf.wert = ghf.wert + ')';
                                    } else {
                                      ghf.wert = ')';
                                    }
                                  },
                                  () {
                                    if (ghf.wert != '0') {
                                      ghf.wert = ghf.wert + 'x';
                                    } else {
                                      ghf.wert = 'x';
                                    }
                                  },
                                  () {
                                    // TODO fix weird problem
                                    if (ghf.wert != '0') {
                                      ghf.wert = ghf.wert + '^';
                                    } else {
                                      ghf.wert = '^';
                                    }
                                  },
                                  () {
                                    ghf.wert = deleteChar(ghf.wert);
                                  },
                                  () {
                                    if ((ghf.mStr(ghf.exerciseiscorrect) +
                                                ghf.aStr(
                                                    ghf.exerciseiscorrect) +
                                                ghf.bStr(ghf.exerciseiscorrect))
                                            .replaceAll(' ', '') ==
                                        ghf.wert.replaceAll(' ', '')) {
                                      onRight();
                                      ghf.wert = '0';
                                      print('correct');
                                    } else {
                                      print('correct = ' +
                                          (ghf.mStr(ghf.exerciseiscorrect) +
                                                  ghf.aStr(
                                                      ghf.exerciseiscorrect) +
                                                  ghf.bStr(
                                                      ghf.exerciseiscorrect))
                                              .replaceAll(' ', ''));
                                      print(ghf.wert.replaceAll(' ', ''));
                                      onWrong();
                                      print('wrong');
                                      ghf.wert = '0';
                                    }
                                  },
                                ],
                                notifyparent: refresh,
                              ),
                            ],
                          ))
          ]);
        }));
  }
}
