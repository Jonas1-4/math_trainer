import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/multiplayer/main_screen/mp_mainscreen.dart';
import 'package:math_trainer/activities/multiplayer/mp_basics_game_handler.dart';
import 'package:math_trainer/activities/multiplayer/mp_game_handler.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/functions/functions_strings.dart';
import 'package:math_trainer/widgets/progressbar.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:math_trainer/widgets/mathGames/KeyboardDir/Keyboard.dart';

class MpBasicsLevelCreator extends StatefulWidget {
  MpBasicsLevelCreator({
    this.operatorStr,
    this.num1,
    this.num2,
    this.color,
    this.seconds,
    this.database,
  });

  Color color;
  String operatorStr;
  int num1, num2, seconds;
  DatabaseReference database;

  @override
  _MpBasicsLevelCreatorState createState() => _MpBasicsLevelCreatorState();
}

class _MpBasicsLevelCreatorState extends State<MpBasicsLevelCreator> {
  List<String> mpScoreList = SharedPrefs().getSpStringList(spPlayedGames + SharedPrefs().getSpStr(spCurrGameID));
  String result;
  Timer _timer;
  MpBasicsGameHandler gh;
  double currentSeconds, maxSeconds; //max seconds stores the start amount for progressbar

  @override
  void initState() {
    maxSeconds = widget.seconds ?? 10;
    currentSeconds = maxSeconds ?? 10;
    startTimer();
    super.initState();
  }

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (currentSeconds <= 0) {
          setState(() {
            buttonsdisabled = true;
            gh.right = false;
            gh.wert = result;
            timer.cancel();
          });
        } else {
          setState(() {
            currentSeconds -= 1;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (gh == null) {
      gh = new MpBasicsGameHandler();
      gh.num1 = widget.num1 ?? 0;
      gh.num2 = widget.num2 ?? 0;
      gh.operatorStr = widget.operatorStr ?? '+';
      result = gh.resultBasic.toString();
    }

    refresh() {
      if(mounted){
      setState(() {});
      }
    }

    enter() {
      _timer.cancel();
      if (gh.wert != result) {
        buttonsdisabled = true;
        gh.right = false;
        gh.wert = result;
        refresh();
      } else {
        gh.right = true;
        buttonsdisabled = true;
        refresh();
      }
    }

    if (mpScoreList.length > 4) {
      _timer.cancel();
      FirebaseDatabase.instance
          .reference()
          .child('Games')
          .child(SharedPrefs().getSpStr(spCurrGameID))
          .update({FirebaseAuth.instance.currentUser.uid: mpScoreList.toString()});
      runApp(MpMainScreen());
    }

    if (buttonsdisabled) {
      if (gh.color() == color_right[theme]) {
        // TODO change to 3 seconds (wrong)
        Future.delayed(Duration(seconds: 1), () {
          mpScoreList.add('1');
          print(mpScoreList);
          // TODO check params of str list
          SharedPrefs().setSpStrList(spPlayedGames + SharedPrefs().getSpStr(spCurrGameID), mpScoreList);
          gh = null;
          buttonsdisabled = false;
          currentSeconds = maxSeconds;
          startTimer();
          MpGamehandler().runMpGame(widget.database.parent().child(mpScoreList.length.toString()));
        });
      } else if (gh.color() == color_wrong[theme]) {
        Future.delayed(Duration(seconds: 1), () {
          mpScoreList.add('0');
          SharedPrefs().setSpStrList(spPlayedGames + SharedPrefs().getSpStr(spCurrGameID), mpScoreList);
          gh = null;
          buttonsdisabled = false;
          currentSeconds = maxSeconds;
          startTimer();
          MpGamehandler().runMpGame(widget.database.parent().child(mpScoreList.length.toString()));
        });
      }
    }

    return StandartApp(
      onWillPop: () {
        mpScoreList.add('0');
        SharedPrefs().setSpStrList(spPlayedGames + SharedPrefs().getSpStr(spCurrGameID), mpScoreList);
        gh = null;
        buttonsdisabled = false;
        SharedPrefs().setSpStrList(spGameIDs, []);
        runApp(HomeScreen());
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Progressbar(
            color: widget.color,
            value: (currentSeconds / maxSeconds).toDouble(),
            padding: 8,
          ),
          SizedBox(
            height: 100,
          ),
          RichText(
              text: TextSpan(
            text: gh.exercise,
            style: TextStyle(fontSize: 40, color: widget.color),
            children: <TextSpan>[
              TextSpan(text: gh.wert.toString(), style: TextStyle(color: (gh.color() ?? widget.color), fontSize: 40)),
            ],
          )),
          Expanded(
            child: Keyboard(
              color: widget.color,
              key: widget.key,
              notifyparent: refresh,
              gamehandler: gh,
              buttons: [
                ['-'],
                [7, 8, 9],
                [4, 5, 6],
                [1, 2, 3],
                ['Del', 0, 'Ent'],
              ],
              stringFunctions: [
                () {
                  if (gh.wert == '0') {
                    gh.wert = '-';
                    refresh();
                  }
                },
                () {
                  gh.wert = deleteChar(gh.wert);
                },
                enter
              ],
            ),
          ),
        ],
      ),
    );
  }
}
