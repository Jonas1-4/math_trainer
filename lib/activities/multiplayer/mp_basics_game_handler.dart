import 'dart:ui';

import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';

class MpBasicsGameHandler {
  bool right;
  int num1 = 0, num2 = 0, num3 = 0, num4 = 0, seconds = 0;
  String wert = '0';
  String exercise = '';
  String operatorStr;

  Color color() {
    return (right == null)
        ? null
        : (right)
            ? color_right[theme]
            : color_wrong[theme];
  }

  int get resultBasic {
    int resultGH;
    print(num1.toString() + ' + ' + num2.toString());
    switch (operatorStr) {
      case '+':
        {
          exercise = num1.toString() + ' + ' + num2.toString() + ' = ' + '';
          resultGH = num1 + num2;
        }
        break;
      case '-':
        {
          exercise = num1.toString() + ' - ' + num2.toString() + ' = ';
          resultGH = num1 - num2;
        }
        break;
      case '*':
        {
          exercise = num1.toString() + ' * ' + num2.toString() + ' = ';
          resultGH = num1 * num2;
        }
        break;
      case '/':
        {
          exercise = num1.toString() + ' : ' + num2.toString() + ' = ';
          resultGH = (num1 / num2).round();
        }
        break;

      default:
        {
          exercise = num1.toString() + ' + ' + num2.toString() + ' = ';
          resultGH = num1 + num2;
        }
    }
    return resultGH;
  }
}
