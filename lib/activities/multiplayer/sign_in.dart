import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/multiplayer/main_screen/mp_mainscreen.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/firebase/authenticate.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

void mpMainScreen() async {
  await Firebase.initializeApp();
  return runApp(SignIn());
}

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  uploadUser(User user) {
    DatabaseReference database =
        FirebaseDatabase.instance.reference().child('Users').child(user.uid);
    database.update({'displayName': user.displayName});
    database.update({'photoURL': user.photoURL});
    database.update({'email': user.email});
    database.update({'score': 50});
  }

  @override
  Widget build(BuildContext context) {
    return StandartApp(
      color: color_background_main_title[theme],
      onWillPop: () {
        runApp(HomeScreen());
      },
      child: Builder(builder: (BuildContext context) {
        return Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FlatButton(
                  onPressed: () {
                    // TODO make ProfilePicture edible
                    runApp(
                      SignUpEmail(),
                    );
                  },
                  child: Text(
                    'Sign up/in with Email',
                    style: TextStyle(
                      color: color_background_main_title[theme],
                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                FlatButton(
                  onPressed: () {
                    AuthenticationService(FirebaseAuth.instance)
                        .signInGoogle()
                        .then((result) {
                      if (result != null) {
                        uploadUser(FirebaseAuth.instance.currentUser);
                        return runApp(MpMainScreen());
                      } else {}
                    });
                  },
                  child: Text('Google Sign-In',
                      style: TextStyle(
                        color: color_background_main_title[theme],
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      )),
                ),
                SizedBox(
                  height: 100,
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

}

class SignUpEmail extends StatefulWidget {
  @override
  _SignUpEmailState createState() => _SignUpEmailState();
}

class _SignUpEmailState extends State<SignUpEmail> {
  final _formKey = GlobalKey<FormState>();
  String name, email, password, passwordCheck, error = '';
  bool _isObscure = true;
  Map<dynamic, dynamic> users;
  uploadUser(User user) {
    DatabaseReference database =
    FirebaseDatabase.instance.reference().child('Users').child(user.uid);
    database.update({'displayName': user.displayName});
    database.update({'photoURL': user.photoURL});
    database.update({'email': user.email});
    database.update({'score': 50});
  }
  @override
  Widget build(BuildContext context) {
    FirebaseDatabase.instance.reference().child('Users').once().then((value) {
      users = value.value;});
    return StandartApp(
        onWillPop: () {
          runApp(HomeScreen());
        },
        color: color_background_main_title[theme],
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      style: TextStyle(color: color_background_main_title[theme]),
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: color_hints[theme])),
                          focusColor: color_hints[theme],
                          labelText: 'Enter your username',
                          labelStyle: TextStyle(color: color_hints[theme])),
                      cursorColor: color_background_main[theme],
                      validator: (val) {
                        if (val.isEmpty) {
                          return 'Please enter an Username';
                        }
                        for (var i in users.values) {
                          if (i['displayName'].toString().toLowerCase() ==
                              val.toLowerCase()) {
                            return 'Username is taken';
                          }
                        }
                        name = val;
                        return null;
                      },
                      onFieldSubmitted: (val) {
                        name = val;
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      style: TextStyle(color: color_background_main_title[theme]),
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: color_hints[theme])),
                          focusColor: color_hints[theme],
                          labelText: 'Enter your Email',
                          labelStyle: TextStyle(color: color_hints[theme])),
                      cursorColor: color_background_main[theme],
                      validator: (val) {
                        if (val.isEmpty) {
                          return 'Please enter an Email';
                        }
                        for (var i in users.values) {
                          if (i['email'].toString().toLowerCase() ==
                              val.toLowerCase()) {
                            return 'Email is used already';
                          }
                          if (!EmailValidator.validate(val.trim())) {
                            return 'Please enter a valid Email';
                          }
                        }
                        email = val.trim();
                        return null;
                      },
                      onFieldSubmitted: (val) {
                        email = val.trim();
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      obscureText: _isObscure,
                      style: TextStyle(color: color_background_main_title[theme]),
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                              icon: Icon(_isObscure
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              onPressed: () {
                                setState(() {
                                  _isObscure = !_isObscure;
                                });
                              }),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: color_hints[theme])),
                          focusColor: color_hints[theme],
                          labelText: 'Enter your Password',
                          labelStyle: TextStyle(color: color_hints[theme])),
                      cursorColor: color_background_main[theme],
                      validator: (val) {
                        if (val.length <= 6) {
                          return 'Passwords need to be atleast 6 characters long';
                        }
                        password = val;
                        return null;
                      },
                      onFieldSubmitted: (val) {
                        password = val;
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                      style: TextStyle(color: color_background_main_title[theme]),
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: color_hints[theme])),
                          focusColor: color_hints[theme],
                          labelText: 'Confirm your Password',
                          labelStyle: TextStyle(color: color_hints[theme])),
                      validator: (val) {
                        if (val != password) {
                          return 'Passwords do not match';
                        }
                        if (password.isEmpty) {
                          return 'Please enter a password first';
                        }
                        passwordCheck = val;
                        return null;
                      },
                      cursorColor: color_background_main[theme],
                      onFieldSubmitted: (val) {
                        passwordCheck = val;
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                    onTap: () {
                      runApp(SignInEmail());
                    },
                    child: Row(
                      children: [Text('Already have an Account?')],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(error,
                          style: TextStyle(
                            color: color_wrong[theme],
                          )),
                      Expanded(child: Container()),
                      RaisedButton(
                        child: Text('Submit'),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            AuthenticationService(FirebaseAuth.instance)
                                .signUpEmail(email: email, password: password)
                                .then((result) {
                              FirebaseAuth.instance.currentUser
                                  .updateProfile(displayName: name)
                                  .then(
                                    (result) {
                                  uploadUser(FirebaseAuth.instance.currentUser);
                                },
                              );
                              runApp(HomeScreen());
                            });
                          }
                        },
                        color: color_themes[1][theme],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
  }
}


class SignInEmail extends StatefulWidget {
  @override
  _SignInEmailState createState() => _SignInEmailState();
}

class _SignInEmailState extends State<SignInEmail> {
  final _formKey = GlobalKey<FormState>();
  String email, password, error = '';
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return StandartApp(
        onWillPop: () {
          runApp(HomeScreen());
        },
        color: color_background_main_title[theme],
        child: Form(
            key: _formKey,
            child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                        style: TextStyle(
                            color: color_background_main_title[theme]),
                        decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: color_hints[theme])),
                            focusColor: color_hints[theme],
                            labelText: 'Enter your email',
                            labelStyle: TextStyle(color: color_hints[theme])),
                        cursorColor: color_background_main[theme],
                        validator: (val) {
                          if (val.isEmpty) {
                            return 'Please enter an Email';
                          }
                          email = val;
                          return null;
                        },
                        onFieldSubmitted: (val) {
                          email = val;
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      obscureText: _isObscure,
                        style: TextStyle(
                            color: color_background_main_title[theme]),
                        decoration: InputDecoration(
                            suffixIcon: IconButton(
                                icon: Icon(_isObscure
                                    ? Icons.visibility
                                    : Icons.visibility_off),
                                onPressed: () {
                                  setState(() {
                                    _isObscure = !_isObscure;
                                  });
                                }),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: color_hints[theme])),
                            focusColor: color_hints[theme],
                            labelText: 'Enter your Password',
                            labelStyle: TextStyle(color: color_hints[theme])),
                        cursorColor: color_background_main[theme],
                        validator: (val) {
                          if (val.isEmpty) {
                            return 'Please enter an Password';
                          }
                          password = val;
                          return null;
                        },
                        onFieldSubmitted: (val) {
                          password = val;
                        }),
                  ),
                  Text(
                    error,
                    style: TextStyle(color: color_wrong[theme]),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(child: Container()),
                        RaisedButton(
                          child: Text('Submit'),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              AuthenticationService(FirebaseAuth.instance)
                                  .signInEmail(email: email, password: password)
                                  .then((result) {
                                if (result == "Signed in") {
                                  runApp(MpMainScreen());
                                } else {
                                  setState(() {
                                    error = result;
                                  });
                                }
                              });
                            }
                          },
                          color: color_themes[1][theme],
                        ),
                      ],
                    ),
                  )
                ]))));
  }
}
