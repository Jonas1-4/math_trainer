import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/aftermultiplayer_screen.dart';
import 'package:math_trainer/activities/multiplayer/main_screen/mp_mainscreen.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/data/providers/firebasedatabase_provider.dart';
import 'package:math_trainer/functions/shared_prefs.dart';

import '../mp_game_handler.dart';

Future<List<Widget>> generateCardList() async {
  List<dynamic> games = await _findGames();
  List<Widget> returnList = [];
  DatabaseReference database = FirebaseDatabase.instance.reference();
  ImageProvider oppPp, ownPp;
  ownPp = await FirebaseRepository().getUserPb(FirebaseAuth.instance.currentUser.uid);

  if (games == null) {
    return [];
  }
  for (Map i in games) {

    Map gameMap = await FirebaseRepository().getGame(i['gameID']);
    oppPp = await FirebaseRepository().getUserPb(i['uid']);
    List<String> mpScoreList =
    SharedPrefs().getSpStringList(spPlayedGames + i['gameID']);
    if(gameMap['canDel'] != FirebaseAuth.instance.currentUser.uid ){
    returnList.add(InkWell(
      onTap: () {
        MpGamehandler mpgh = new MpGamehandler();
        SharedPrefs().setSpStr(spCurrGameID, i['gameID']);
        if (mpScoreList.length != 5) {
          mpgh.runMpGame(database
              .child('Games')
              .child(i['gameID'])
              .child('GameList')
              .child((mpScoreList.length).toString()));
        } else if (mpScoreList.length == 5 &&
            gameMap[SharedPrefs().getSpStr('opponent' + i["gameID"])] !=
                null) {
          afterMultiplayerScreenHandle();
          runApp(AfterMultiplayerScreen(
              ownPb: ownPp,
              oppPb: oppPp,
              ownScore: mpScoreList,
              oppScore: json
                  .decode(gameMap[i['uid']]),
              ));
        }},
      child: MpGameMenuCard(
          image: oppPp, displayName: i['displayName'], opponentPlayed: i['opponentPlayed'], gameID: i['gameID']),
    ));
  }}
  return returnList;
}

Future<dynamic> _findGames() async {
  List<String> gameIDs = SharedPrefs().getSpStringList(spGameIDs), mpScoreList;
  DatabaseReference database = FirebaseDatabase.instance.reference();
  String opponentUid;
  Map foundGamesMap = await FirebaseRepository().getGames();
  Map oppMap, foundGameMap;
  List games = [];
  String uid = FirebaseAuth.instance.currentUser.uid;
  if (foundGamesMap != null) {
    for (dynamic i in foundGamesMap.keys) {
      print('i:' + i.toString());
      if (!gameIDs.contains(i)) {
        SharedPrefs().setSpBool('isSearching', false);
      }
      if (foundGamesMap[i]['player1'] == uid || foundGamesMap[i]['player2'] == uid) {
        foundGameMap = await FirebaseRepository().getGame(i);
        String opponentUid = foundGameMap['player1'] == uid ? foundGameMap['player2'] : foundGameMap['player1'];
        oppMap = await FirebaseRepository().getUser(opponentUid);
        mpScoreList = SharedPrefs().getSpStringList(spPlayedGames + i);
        if (foundGamesMap[i]['player1'] == uid) {
          SharedPrefs().setSpStr('opponent' + i, foundGamesMap[i]['player2']);
          print('isHost');
          if (foundGameMap['GameList'] == null) {
            MpGamehandler().generateMpGame(database.child('Games').child(i).child('GameList'));
          }
          SharedPrefs().addSpStringList(spGameIDs, i);
          print('\nHost added Game\n');
          games.add({
            'uid': opponentUid,
            'image': oppMap["photoURL"],
            'displayName': oppMap["displayName"],
            'gameID': i,
            'opponentPlayed':
                mpScoreList.length == 5 && foundGamesMap[i][SharedPrefs().getSpStr('opponent' + i)] != null,
          });
        } else {
          print('isGuest');
          SharedPrefs().setSpStr('opponent' + i, foundGamesMap[i]['player1']);
            SharedPrefs().addSpStringList(spGameIDs, i);
            print('\nGuest added Game\n');
            games.add({
              'uid': opponentUid,
              'image': oppMap["photoURL"],
              'displayName': oppMap["displayName"],
              'gameID': i,
              'opponentPlayed':
                  mpScoreList.length == 5 && foundGamesMap[i][SharedPrefs().getSpStr('opponent' + i)] != null,
            });
        }
      }
    }
  }
  return games;
}

afterMultiplayerScreenHandle() async {
  DatabaseReference databaseReference = FirebaseDatabase.instance.reference();
  String gameID = SharedPrefs().getSpStr(spCurrGameID), uid = FirebaseAuth.instance.currentUser.uid;
  int ownScoreInt = 0, opponentScoreInt = 0, score = 0;
  List<dynamic> ownScore = [], opponentScore = [];

  databaseReference.child('Games').child(gameID).once().then((data) {
    Map<dynamic, dynamic> gamedata = data.value;
    opponentScore = json.decode(gamedata[SharedPrefs().getSpStr('opponent' + gameID)]);
    ownScore = json.decode(gamedata[uid]);
    for (int i = 0; i <= opponentScore.length - 1; i++) {
      ownScoreInt += ownScore[i];
      opponentScoreInt += opponentScore[i];
    }
    databaseReference.child('Users').child(uid).once().then((value) {
      score = value.value['score'];
      databaseReference.child('Users').child(uid).update({'score': score + (ownScoreInt - opponentScoreInt)});
    });
    if (gamedata['canDel'] == null) {
      databaseReference.child('Games').child(gameID).update({'canDel': uid});
      SharedPrefs().setSpStrList(spPlayedGames + gameID, null);
      SharedPrefs().setSpStrList(spGameIDs, []);
    } else if (gamedata['canDel'] != uid) {
      databaseReference.child('Games').child(gameID).remove();
      SharedPrefs().setSpStrList(spPlayedGames + gameID, null);
      SharedPrefs().setSpStrList(spGameIDs, []);
    }
    return;
  });
}
