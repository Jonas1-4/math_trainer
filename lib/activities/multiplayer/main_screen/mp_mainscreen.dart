import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/multiplayer/main_screen/mp_mainscreen_functions.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/data/providers/firebasedatabase_provider.dart';
import 'package:math_trainer/functions/functions_multiplayer.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/widgets/multiplayer_drawer.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class MpMainScreen extends StatefulWidget {
  @override
  _MpMainScreenState createState() => _MpMainScreenState();
}

class _MpMainScreenState extends State<MpMainScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DatabaseReference database = FirebaseDatabase().reference();
  ImageProvider ownPp;
  String uid = FirebaseAuth.instance.currentUser.uid;

  fetchGames() async {
    foundGamesCards = await generateCardList();
    if (mounted) {
      setState(() {});
    }
    return;
  }

  setPp() async {
    ownPp = await FirebaseRepository().getUserPb(uid);
    if (mounted) {
      setState(() {});
    }
    return;
  }

  @override
  void initState() {
    super.initState();
    fetchGames();
    setPp();
    return;
  }

  MultiplayerFunctions mpf = new MultiplayerFunctions();

  List<Widget> foundGamesCards = [];

    @override
  Widget build(BuildContext context) {
    Color color = color_background_main_subtitle[theme];
    String uid = FirebaseAuth.instance.currentUser.uid;
    bool searching = SharedPrefs().getSpBool('isSearching');
    setSearching(bool b) {
      SharedPrefs().setSpBool('isSearching', b);
      if (mounted) {
        setState(() {});
      }
    }
    if(searching){
      database.child('Games').onChildAdded.listen((event) {
        print('A Game got created');
        fetchGames();
      });
    }




    Map usersMap;
    return MaterialApp(
        home: WillPopScope(
            // ignore: missing_return
            onWillPop: () {
              SharedPrefs().setSpStrList(spGameIDs, []);
              runApp(HomeScreen());
            },
            child: Scaffold(
                key: _scaffoldKey,
                appBar: AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  title: Row(children: [
                    IconButton(
                        icon: Icon(
                          Icons.view_headline,
                          color: color_background_main_subtitle[theme],
                        ),
                        onPressed: () async {
                          usersMap = await database.child('Users').once().then((value) {
                            return value.value;
                          });
                          return _scaffoldKey.currentState.openDrawer();
                        })
                  ]),
                  actions: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.close,
                        color: color_background_main_title[theme],
                      ),
                      onPressed: () {
                        SharedPrefs().setSpStrList(spGameIDs, []);
                        runApp(HomeScreen());
                      },
                      color: color_background_main_subtitle[0],
                    )
                  ],
                ),
                drawer: MultiPlayerDrawer(
                  users: usersMap,
                  setParentState: () {},
                ),
                backgroundColor: color_background_main[theme],
                // TODO make it so it does only check database 'QueQue' + 'Games'
                body: RefreshIndicator(
                    onRefresh: () async {
                      await fetchGames();
                      return;
                    },
                    child: Stack(
                      children: [
                        ListView(),
                        Builder(
                          builder: (context) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                //Profile pic
                                Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: InkWell(
                                        onTap: () async {}, child: CircleAvatar(radius: 40, backgroundImage: ownPp))),
                                Container(
                                  height: 100,
                                ),
                                (searching)
                                    ? MpSearchMenuCard(
                                        database: database,
                                        uid: uid,
                                      )
                                    : Container(),
                                Expanded(
                                  child: ListView(
                                    children: [
                                      Column(children: foundGamesCards ?? [Container()]),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Center(
                                  child: InkWell(
                                    onTap: () {
//                                        SharedPrefs().setSpStrList('MpScoreList', []);
                                      setSearching(true);
                                      database.child('QueQue').update({uid + '0': "Searching"});
                                    },
                                    child: Card(
                                        color: color_background_main[theme],
                                        elevation: 0,
                                        child: (!searching)
                                            ? Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Text(
                                                  'Search Game',
                                                  style: TextStyle(
                                                      color: color, fontSize: ResponsiveFlutter.of(context).fontSize(4)),
                                                ),
                                              )
                                            : Container()),
                                  ),
                                ),
                                SizedBox(
                                  height: ResponsiveFlutter.of(context).moderateScale(10, 10),
                                )
                              ],
                            );
                          }
                        ),
                      ],
                    )))));
  }
}

class MpSearchMenuCard extends StatelessWidget {
  const MpSearchMenuCard({
    Key key,
    @required this.database,
    @required this.uid,
  }) : super(key: key);

  final DatabaseReference database;
  final String uid;

  @override
  Widget build(BuildContext context) {
    Color color = color_background_main_title[theme];
    return SizedBox(
        height: 120,
        child: Padding(
            padding: const EdgeInsets.all(18),
            child: Center(
                child: Card(
                    color: color_background_main[theme],
                    elevation: 5,
                    child: Row(children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation(color),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Searching for Game',
                          style: TextStyle(
                            color: color_hints[theme],
                            fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.close,
                            color: color,
                          ),
                          onPressed: () {
                            database.child('QueQue').child(uid + '0').remove();
                            SharedPrefs().setSpBool('isSearching', false);
                            runApp(MpMainScreen());
                          })
                    ])))));
  }
}

class MpGameMenuCard extends StatelessWidget {
  const MpGameMenuCard({
    Key key,
    @required this.color,
    @required this.image,
    @required this.displayName,
    @required this.opponentPlayed,
    @required this.gameID,
  }) : super(key: key);

  final Color color;
  final String displayName, gameID;
  final ImageProvider image;
  final bool opponentPlayed;

  @override
  Widget build(BuildContext context) {
    ImageProvider pb = image ?? AssetImage('assets/anonym.png');
    List<CircleAvatar> playedGames = [];
    List<String> mpScoreList = SharedPrefs().getSpStringList(spPlayedGames + gameID);
    for (int i = mpScoreList.length; i < 5; i++) {
      mpScoreList.add('2');
    }
    for (int i = 0; i < 5; i++) {
      Color color = mpScoreList[i] == '2'
          ? color_hints[theme]
          : mpScoreList[i] == '0'
              ? color_wrong[theme]
              : color_right[theme];
      playedGames.add(CircleAvatar(
        radius: 10,
        backgroundColor: darken(color_background_main[theme], 30),
        child: CircleAvatar(radius: 8, backgroundColor: color),
      ));
    }
    return SizedBox(
        height: 120,
        child: Padding(
          padding: const EdgeInsets.all(18),
          child: Center(
              child: Card(
            color: color_background_main[theme],
            elevation: 5,
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: CircleAvatar(
                    backgroundImage: pb,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      displayName,
                      style: TextStyle(
                        color: color_hints[theme],
                        fontSize: ResponsiveFlutter.of(context).fontSize(3),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      for (var i in playedGames)
                        Padding(
                          padding: const EdgeInsets.all(1.0),
                          child: i,
                        ),
                    ],
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.notification_important,
                    color: (opponentPlayed) ? color_wrong[theme] : color_background_main[theme],
                  ),
                )
              ],
            ),
          )),
        ));
  }
}
