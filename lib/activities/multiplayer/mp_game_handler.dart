import 'dart:math';
import 'dart:ui';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'levels/basics.dart';

class MpGamehandler{
  List<dynamic> games;
  List<bool> scores;
  String gameId, uid, uidOponent, displaynameOponent;
  bool isHost;
  Color color;

void generateMpGame(DatabaseReference database) {
    print('generatingGame');
    Random random = new Random();
    for (int i = 0; i < 5; i++) {
      int game = random.nextInt(1);
      //first switch defines the theme second the specific level
      switch (game) {
        case 0:
          {
            int num1, num2;
            game = random.nextInt(4);
            switch (game){
              case 0:
                 num1 = random.nextInt(2000) - 1000;
                 num2 = random.nextInt(2000) - 1000;
                 break;
              case 1:
                num1 = random.nextInt(1500);
                num2 = random.nextInt(1500);
                break;
              case 2:
                num2 = random.nextInt(100) - 50;
                (num2 ==0)? num2 = num2 + random.nextInt(100)+2:num2=num2;
                num1 = random.nextInt(20) * num2;
                break;
              case 3:
                num1 = random.nextInt(100) - 50;
                num2 = random.nextInt(50) - 25;
                break;
            }

            database.child(i.toString()).update({
              'game': game,
              'num1': num1,
              'num2': num2
            });
            break;
          }
        case 1:
          {
            }
            break;
          }
      }
    }
    
    void runMpGame(DatabaseReference database){
      database.once().then((data) {
        print(data.value['game']);
          switch (data.value['game']){
            case 0:
              {
                runApp(MpBasicsLevelCreator(color: color_themes[0][theme],operatorStr: '+', num1: data.value['num1'], num2: data.value['num2'],database: database, ));
              }
              break;
            case 1:
              {
                runApp(MpBasicsLevelCreator(color: color_themes[0][theme],operatorStr: '-', num1: data.value['num1'], num2: data.value['num2'],database: database, ));
              }
              break;
            case 2:
              {
                runApp(MpBasicsLevelCreator(color: color_themes[0][theme],operatorStr: '/', num1: data.value['num1'], num2: data.value['num2'],database: database,));
              }
              break;
            case 3:
              {
                runApp(MpBasicsLevelCreator(color: color_themes[0][theme],operatorStr: '*', num1: data.value['num1'], num2: data.value['num2'],database: database,));
              }
              break;
            default:
              {
                runApp(MpBasicsLevelCreator(color: color_themes[0][theme],operatorStr: '+', num1: 234, num2: 777,database: database,));
              }
          }
      });
    }

  }

