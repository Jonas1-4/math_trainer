import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/widgets/standart_app.dart';

class GeometryLevelCreator extends StatelessWidget {
  GeometryLevelCreator({
    this.color,
  });
  Color color;

  @override
  Widget build(BuildContext context) {
    return StandartApp(
      color: color,
      onWillPop: () {
        runApp(HomeScreen());
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomPaint(
            painter: CubeGeometry(height: 5, width: 5, color: color),
          ),
        ],
      ),
    );
  }
}

class CubeGeometry extends CustomPainter {
  CubeGeometry({this.color, this.height, this.width});
  double height;
  double width;
  Color color;
  @override
  void paint(Canvas canvas, Size size) {
    final rectPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeJoin = StrokeJoin.round
      ..strokeCap = StrokeCap.round
      ..color = color;
    var center = Offset(size.width / 2 - width * 10, size.height / 2);
    canvas.drawLine(Offset(width * 10 + 5, 0),
        Offset(width * 10 + 5, height * 10), rectPaint);
    canvas.drawRect(center & Size(height * 10, width * 10), rectPaint);

    //canvas.drawParagraph('jaja', paint)
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}
