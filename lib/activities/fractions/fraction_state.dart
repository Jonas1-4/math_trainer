part of 'fraction_bloc.dart';

abstract class FractionState extends Equatable {
  @override
  List<Object> get props => [];
}

class FractionInitial extends FractionState {
  final int maxLevel, numLimitBot, numLimitTop;
  final String operatorStr, gameMode;
  final Color color;
  final bool minus, sameNuminator;

  FractionInitial(
     {this.maxLevel,
      this.numLimitBot,
      this.numLimitTop,
      this.sameNuminator,
      this.color,
      this.operatorStr,
      this.gameMode,
      this.minus});
}

class ShowExercise extends FractionState {
  @override
  List<Object> get props => [exercise];

  final String exercise;

  ShowExercise({
    this.exercise,
  });
}

class ShowFractionUserInput extends FractionState {
  @override
  List<Object> get props => [userInput];

  final String userInput;

  ShowFractionUserInput({
    this.userInput,
  });
}

class ShowAnswer extends FractionState {
  final Color colorInput;
  final String correctAnswer;
  final bool maxScore;

  ShowAnswer({this.correctAnswer, this.colorInput, this.maxScore});
}

class InitiateNewExercise extends FractionState {
  final int numLimit;
  final String operatorStr;

  InitiateNewExercise({this.operatorStr, this.numLimit});
}
