import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/data/keyboard_level_model.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/functions/functions_strings.dart';
import 'package:meta/meta.dart';

part 'fraction_event.dart';
part 'fraction_state.dart';

//really dont know if this is good practice but think it organizes the bloc.
String _operatorStr, _correctAnswer;
int _num1Top, _num1Bot, _num2Top, _num2Bot;


class FractionBloc extends Bloc<FractionEvent, FractionState> {
  FractionBloc(FractionState initialState) : super(initialState);
  KeyboardLevelModel fractionModel;
  double solutionFractions;
  String inputTop = '0', inputBot = '0';
  bool num2sel;
  @override
  Stream<FractionState> mapEventToState(
    FractionEvent event,
  ) async* {
    if (event is InitiateFraction) {
      print('event.maxlevel: ${event.maxLevel}');
      fractionModel ??= new KeyboardLevelModel(
          operatorStr: event.operatorStr,
          maxLevel: event.maxLevel,
          minus: event.minus ?? false);
      fractionModel.reset();
      inputTop = '0';
      inputBot = '0';
      num2sel = false;
      _operatorStr = event.operatorStr;

      fractionModel.exerciseNumbers = _generateNumbers(event.numLimitBot,
          event.numLimitTop, event.sameNuminator, event.minus);
      solutionFractions = _generateSolution(fractionModel.exerciseNumbers);

      
      fractionModel.exercise =
          '\\sf{\\frac{${fractionModel.exerciseNumbers[0]}}' +
              '{${fractionModel.exerciseNumbers[1]}}' +
              '${fractionModel.operatorStr}' +
              '\\frac{${fractionModel.exerciseNumbers[2]}}' +
              '{${fractionModel.exerciseNumbers[3]}} =}';
      print(fractionModel.exercise);
      print('numLimitTop: ${event.numLimitTop} ' +
          ' numLimitBot: ${event.numLimitBot} ' +
          ' operatorStr: ${event.operatorStr} ' +
          ' solution: ${fractionModel.solution}');
      yield ShowExercise(exercise: fractionModel.exercise);
    }

    if (event is ChangeFractionValue) {
      if (event.numberPressed == '/') {
        print("pressed");
        num2sel = true;
        fractionModel.input = '\\frac{${inputTop}}{${inputBot}}';
        yield ShowFractionUserInput(userInput: fractionModel.input);
      } else {
        print('no');
        if (!num2sel) {
          if (inputTop == '0') {
            inputTop = event.numberPressed;
            fractionModel.input = inputTop;
          } else {
            inputTop = inputTop + event.numberPressed;
            fractionModel.input = inputTop;
          }
          yield ShowFractionUserInput(userInput: fractionModel.input);
        } else {
          if (inputBot == '0') {
            inputBot = event.numberPressed;
          } else {
            inputBot = inputBot + event.numberPressed;
          }
          fractionModel.input = '\\frac{${inputTop}}{${inputBot}}';
          yield ShowFractionUserInput(userInput: fractionModel.input);
        }
      }
    }

    if (event is DeleteFractionValue) {
      fractionModel.input = deleteChar(fractionModel.input);

      if (!num2sel) {
        inputTop = deleteChar(inputTop);
        fractionModel.input = inputTop;
      } else {
        if (inputBot == '0') {
          num2sel = false;
          fractionModel.input = inputTop;
        } else {
          inputBot = deleteChar(inputBot);
          fractionModel.input = '\\frac{${inputTop}}{${inputBot}}';
        }
      }
      yield ShowFractionUserInput(userInput: fractionModel.input);
    }

    if (event is SubmitFractionValue) {
      Color colorInput = color_wrong[theme];
      bool maxScore = false;
      print(
          'score: ${fractionModel.score} maxlevel: ${fractionModel.maxLevel}');
      if (fractionModel.solutionFractions ==
          (int.parse(inputTop) / int.parse(inputBot))) {
        fractionModel.score++;
        colorInput = color_right[theme];
        if (fractionModel.score >= fractionModel.maxLevel) {
          maxScore = true;
          SharedPrefs().increaseSpInt(currentTopic + 'percentage', 1);
        }
      } else {
        fractionModel.score--;
      }
      yield ShowAnswer(
          correctAnswer: "\\frac$_correctAnswer",
          colorInput: colorInput,
          maxScore: maxScore);
    }

    if (event is GenerateFractionExercise) {
      yield FractionInitial();
    }
  }
}

List<int> _generateNumbers(
    int numLimitBot, int numLimitTop, bool sameNuminator, bool minus) {
  List<int> exerciseNumbers;
  Random random = new Random();
  int biggernum;
  //switch statement is needed for number generation
  _num1Top = random.nextInt(numLimitTop) + 1;
  _num2Top = random.nextInt(numLimitTop) + 1;
  biggernum = _num1Top > _num2Top ? _num1Top : _num2Top;
  _num1Bot = sameNuminator
      ? random.nextInt(numLimitBot) + biggernum
      : random.nextInt(numLimitBot) + 1;
  _num2Bot = sameNuminator ? _num1Bot : random.nextInt(numLimitBot) + 1;
  if (minus) {
    _num1Top = _num1Top - (numLimitTop / 2).round();
    _num2Top = _num2Top - (numLimitTop / 2).round();
    // I dont think I need:
    //num1Bot = num1Bot - (event.numLimitBot / 2).round();
    //num2Bot = num2Bot - (event.numLimitBot / 2).round();
  }
  if (_operatorStr == '/') {
    exerciseNumbers.add(_num1Top * _num2Top);
    exerciseNumbers.add(_num1Bot * _num2Bot);
    exerciseNumbers.add(_num2Top);
    exerciseNumbers.add(_num2Bot);
  } else {
    exerciseNumbers.add(_num1Top);
    exerciseNumbers.add(_num1Bot);
    exerciseNumbers.add(_num2Top);
    exerciseNumbers.add(_num2Bot);
  }
  return exerciseNumbers;
}

double _generateSolution(List<int> exerciseNumbers){
  double solutionFractions;
      switch (_operatorStr) {
        case '+':
          {
            solutionFractions = exerciseNumbers[0] /
                    exerciseNumbers[1] +
                exerciseNumbers[2] /
                    exerciseNumbers[3];
            int sameNuminator = findSameNuminator(
                exerciseNumbers[1],
                exerciseNumbers[3]);
            String sameNuminatorStr = sameNuminator.toString();
            String topNum = ((sameNuminator ~/ _num1Bot * _num1Top) +
                    (sameNuminator ~/ _num2Bot * _num2Top))
                .toString();
            _correctAnswer = '{$topNum}{$sameNuminatorStr}';
          }
          break;
        case '-':
          {
            solutionFractions = exerciseNumbers[0] /
                    exerciseNumbers[1] -
                exerciseNumbers[2] /
                    exerciseNumbers[3];
            int sameNuminator = findSameNuminator(
                exerciseNumbers[1],
                exerciseNumbers[3]);
            String sameNuminatorStr = sameNuminator.toString();
            String topNum = ((sameNuminator ~/ _num1Bot * _num1Top) -
                    (sameNuminator ~/ _num2Bot * _num2Top))
                .toString();
            _correctAnswer = '{$topNum}{$sameNuminatorStr}';
          }
          break;
        case '*':
          {
            solutionFractions = exerciseNumbers[0] /
                exerciseNumbers[1] *
                exerciseNumbers[2] /
                exerciseNumbers[3];
            _correctAnswer = "{${_num1Top * _num2Top}}{${_num1Bot * _num2Bot}}";
          }
          break;
        case '/':
          {
            solutionFractions = exerciseNumbers[0] /
                exerciseNumbers[1] /
                exerciseNumbers[2] /
                exerciseNumbers[3];
            _correctAnswer = "{${_num1Top / _num2Top}}{${_num1Bot / _num2Bot}}";
          }
          break;
      }
      return solutionFractions;
}

