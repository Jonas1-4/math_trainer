import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'fraction_bloc.dart';
import 'fraction_screen.dart';

class FractionInit {
  Widget createFractionScreen(
          {Color color,
          int maxLevel,
          int numLimitTop,
          int numLimitBot,
          bool minus,
          bool sameNuminator,
          String operatorStr,
          String gameMode}) =>
      BlocProvider<FractionBloc>(
          create: (context) => FractionBloc(FractionInitial(
              color: color,
              maxLevel: maxLevel,
              numLimitTop: numLimitTop,
              numLimitBot: numLimitBot,
              minus: minus,
              sameNuminator: sameNuminator,
              operatorStr: operatorStr,
              gameMode: gameMode)),
          child: FractionScreen());
}
