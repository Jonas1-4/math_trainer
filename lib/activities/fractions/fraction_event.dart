part of 'fraction_bloc.dart';

@immutable
abstract class FractionEvent extends Equatable {
  @override
  List<Object> get props => null;
}

class InitiateFraction extends FractionEvent {
  final String operatorStr;
  final int numLimitBot, numLimitTop;
  final int maxLevel;
  final bool minus;
  final bool sameNuminator;
  InitiateFraction(
      {this.operatorStr,
      this.numLimitTop,
      this.numLimitBot,
      this.maxLevel,
      this.minus,
      this.sameNuminator});
}

class ChangeFractionValue extends FractionEvent {
  ChangeFractionValue(
    this.numberPressed,
  );
  final String numberPressed;
}

class DeleteFractionValue extends FractionEvent {}

class GenerateFractionExercise extends FractionEvent {}

class SubmitFractionValue extends FractionEvent {
  final String userInput;
  SubmitFractionValue({
    this.userInput,
  });
}

//Dont know how to call thios it changes number 1 to number 2?
class SelectFractionFraction extends FractionEvent {}


