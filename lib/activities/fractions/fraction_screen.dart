import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/menus/topic_select.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_level.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/functions_games_bloc.dart';
import 'package:math_trainer/widgets/progressbar.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:math_trainer/widgets/mathGames/KeyboardDirBloc/KeyboardBloc.dart';
import 'package:math_trainer/widgets/mathGames/PracticeScoreDisplay.dart';

import 'fraction_bloc.dart';

class FractionScreen extends StatelessWidget {
  FractionScreen({Key key}) : super(key: key);

  FractionBloc _fractionBloc;


  @override
  Widget build(BuildContext context) {
    _fractionBloc = BlocProvider.of<FractionBloc>(context);

    return StandartApp(
      onWillPop: () {
        runApp(HomeScreen());
      },
      child:
          BlocBuilder<FractionBloc, FractionState>(builder: (context, state) {
        return buildFractionScreen(state);
      }),
    );
  }

  int maxLevel, score = 0, numLimitTop, numLimitBot;
  Color color, colorInput;
  String exercise = '', userInput, operatorStr, gameMode;
  bool sameNuminator, minus;

  Widget buildFractionScreen(FractionState state) {
    print(state);

    if (state is FractionInitial) {
      gameMode ??= state.gameMode;
      maxLevel ??= state.maxLevel;
      color ??= state.color;
      colorInput = color;
      numLimitTop ??= state.numLimitTop;
      numLimitBot ??= state.numLimitBot;
      operatorStr ??= state.operatorStr;
      minus ??= state.minus;
      sameNuminator ??= state.sameNuminator ?? false;
      userInput = '0';
      _initiateFraction(
          sameNuminator: sameNuminator,
          numLimitBot: numLimitBot,
          numLimitTop: numLimitTop,
          operatorString: operatorStr,
          maxLevel: maxLevel);
    }

    if (state is ShowExercise) {
      exercise = state.exercise;
    }

    if (state is ShowFractionUserInput) {
      userInput = state.userInput;
    }

    if (state is ShowAnswer) {
      userInput = state.correctAnswer;
      colorInput = state.colorInput;
      _newExercise(
          operatorString: operatorStr,
          colorInput: colorInput,
          maxScore: state.maxScore);
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        (gameMode == 'Normal')
            ? Progressbar(
                color: color,
                value: (score / maxLevel).toDouble(),
                padding: 8,
              )
            : PracticeScoreDisplay(score: score),
        SizedBox(
          height: 100,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DefaultTextStyle.merge(
              child: Math.tex(r'' + exercise, mathStyle: MathStyle.display),
              style: TextStyle(
                fontSize: 40,
                color: color,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            DefaultTextStyle.merge(
              child: Math.tex(r'\sf { ' + userInput + '}'),
              style: TextStyle(
                fontSize: 40,
                color: (colorInput),
              ),
            ),
          ],
        ),
        Expanded(
          child: Container(),
        ),
        KeyboardBloc(
          color: color,
          numberFunction: _changeValue,
          buttons: [
            [
              '-',
              '/',
            ],
            [7, 8, 9],
            [4, 5, 6],
            [1, 2, 3],
            ['Delete', 0, 'Enter']
          ],
          stringFunctions: [
            _changeValue,
            _changeValue,
            _deleteChar,
            _submit,
          ],
        ),
      ],
    );
  }

  void _initiateFraction(
      {int numLimitTop,
      int numLimitBot,
      String operatorString,
      int maxLevel,
      bool sameNuminator,
      bool minus}) {
    _fractionBloc.add(InitiateFraction(
        sameNuminator: sameNuminator,
        minus: minus,
        numLimitBot: numLimitBot,
        numLimitTop: numLimitTop,
        operatorStr: operatorString,
        maxLevel: maxLevel));
  }

  void _deleteChar(String dummy) {
    _fractionBloc.add(DeleteFractionValue());
  }

  void _changeValue(String number) {
    _fractionBloc.add(ChangeFractionValue(number));
  }

  void _submit(String dummy) {
    _fractionBloc.add(SubmitFractionValue());
  }

  void _newExercise(
      {String operatorString, Color colorInput, bool maxScore}) async {
    if (gameMode == 'Normal') {
      if (colorInput == color_right[theme]) {
        score++;
      } else {
        score--;
      }
      if (maxScore) {
        GameFunctions().nextGameLevels(fractionsLevelList);
      }
    } else if (gameMode == 'Practice') {
      if (colorInput == color_right[theme]) {
        score++;
      } else {
        runApp(LevelLayout(trainingmode: fractionsLevelList));
      }
    }

    await Future.delayed(Duration(seconds: 3));
    _fractionBloc.add(GenerateFractionExercise());
    return;
  }
}
