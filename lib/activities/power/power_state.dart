part of 'power_bloc.dart';

abstract class PowerState extends Equatable {
  @override
  List<Object> get props => [];
}

class PowerInitial extends PowerState {
  final int maxLevel, numLimit, numLimitPow;
  final String operatorStr, gameMode;
  final Color color;

  PowerInitial(
     {this.maxLevel,
      this.numLimit,
      this.color,
      this.operatorStr,
      this.numLimitPow,
      this.gameMode});
}

class ShowExercise extends PowerState {
  @override
  List<Object> get props => [exercise];

  final String exercise;

  ShowExercise({
    this.exercise,
  });
}

class ShowPowerUserInput extends PowerState {
  @override
  List<Object> get props => [userInput];

  final String userInput;

  ShowPowerUserInput({
    this.userInput,
  });
}

class ShowAnswer extends PowerState {
  final Color colorInput;
  final String correctAnswer;
  final bool maxScore;

  ShowAnswer({this.correctAnswer, this.colorInput, this.maxScore});
}

class InitiateNewExercise extends PowerState {
  final int numLimit;
  final String operatorStr;

  InitiateNewExercise({this.operatorStr, this.numLimit});
}
