import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/data/keyboard_level_model.dart';
import 'package:math_trainer/functions/shared_prefs.dart';
import 'package:math_trainer/functions/functions_strings.dart';
import 'package:meta/meta.dart';

part 'power_event.dart';
part 'power_state.dart';

class PowerBloc extends Bloc<PowerEvent, PowerState> {
  PowerBloc(PowerState initialState) : super(initialState);
  KeyboardLevelModel powerModel;
  @override
  Stream<PowerState> mapEventToState(
    PowerEvent event,
  ) async* {
    if (event is InitiatePower) {
      print('event.maxlevel: ${event.maxLevel}');
      powerModel ??= new KeyboardLevelModel(
          operatorStr: event.operatorStr,
          maxLevel: event.maxLevel,
          numLimit: event.numLimit);
      powerModel.reset();
      switch (event.operatorStr) {
        case 'root':
          {
            Random random = new Random();
            print(
                'numlimit: ${event.numLimit} operatorStr: ${event.operatorStr}');
            powerModel.exerciseNumbers.add(2);
            powerModel.exerciseNumbers
                .add(pow(random.nextInt(event.numLimit)+1, 2));
            powerModel.exercise =
                '\\sf{\\sqrt[${powerModel.exerciseNumbers[0]}]{${powerModel.exerciseNumbers[1]} =}}';
          }
          break;
        case 'power':
          {
            Random random = new Random();
            print(
                'numlimit: ${event.numLimit} operatorStr: ${event.operatorStr}');
            powerModel.exerciseNumbers.add(random.nextInt(event.numLimit)+1);
            powerModel.exerciseNumbers.add(2);
            powerModel.exercise = 
                '\\sf{${powerModel.exerciseNumbers[0]}^${powerModel.exerciseNumbers[1]} =}';
          }
          break;
      }
      yield ShowExercise(exercise: powerModel.exercise);
    }

    if (event is ChangePowerValue) {
      print('score: ${powerModel.score} maxlevel: ${powerModel.maxLevel}');
      if (powerModel.input == '0') {
        powerModel.input = event.numberPressed;
      } else {
        powerModel.input = powerModel.input + event.numberPressed;
      }
      yield ShowPowerUserInput(userInput: powerModel.input);
    }

    if (event is DeletePowerValue) {
      powerModel.input = deleteChar(powerModel.input);
      yield ShowPowerUserInput(userInput: powerModel.input);
    }

    if (event is SubmitPowerValue) {
      String correctAnswer;
      switch (powerModel.operatorStr) {
        case 'root':
          {
            correctAnswer = pow(powerModel.exerciseNumbers[1],
                    1 / powerModel.exerciseNumbers[0])
                .round()
                .toString();
          }
          break;
        case 'power':
          {
            correctAnswer = pow(powerModel.exerciseNumbers[0],
                    powerModel.exerciseNumbers[1])
                .round()
                .toString();
          }
          break;
      }
      if (correctAnswer == powerModel.input) {
        powerModel.score++;
        print('score: ${powerModel.score} maxlevel: ${powerModel.maxLevel}');
        if (powerModel.score >= powerModel.maxLevel) {
          SharedPrefs().increaseSpInt(currentTopic + 'percentage',1);
          yield ShowAnswer(
              correctAnswer: correctAnswer,
              colorInput: color_right[theme],
              maxScore: true);
        }
        yield ShowAnswer(
            correctAnswer: correctAnswer,
            colorInput: color_right[theme],
            maxScore: false);
      } else {
        yield ShowAnswer(
            correctAnswer: correctAnswer,
            colorInput: color_wrong[theme],
            maxScore: false);
      }
    }

    if (event is GeneratePowerExercise) {
      yield PowerInitial();
    }
  }
}
