part of 'power_bloc.dart';

@immutable
abstract class PowerEvent extends Equatable {
  @override
  List<Object> get props => null;
}

class InitiatePower extends PowerEvent {
  final String operatorStr;
  final int numLimit;
  final int maxLevel;
  InitiatePower({
    this.operatorStr,
    this.numLimit,
    this.maxLevel,
  });
}

class ChangePowerValue extends PowerEvent {
  ChangePowerValue(
    this.numberPressed,
  );
  final String numberPressed;
}

class DeletePowerValue extends PowerEvent {}

class GeneratePowerExercise extends PowerEvent {}

class SubmitPowerValue extends PowerEvent {
  final String userInput;
  SubmitPowerValue({
    this.userInput,
 });
}

