import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:math_trainer/activities/menus/home_screen.dart';
import 'package:math_trainer/activities/menus/topic_select.dart';
import 'package:math_trainer/activities/power/power_bloc.dart';
import 'package:math_trainer/data/data_color.dart';
import 'package:math_trainer/data/data_level.dart';
import 'package:math_trainer/data/data_public_variables.dart';
import 'package:math_trainer/functions/functions_games_bloc.dart';
import 'package:math_trainer/widgets/progressbar.dart';
import 'package:math_trainer/widgets/standart_app.dart';
import 'package:math_trainer/widgets/mathGames/KeyboardDirBloc/KeyboardBloc.dart';
import 'package:math_trainer/widgets/mathGames/PracticeScoreDisplay.dart';



class PowerScreen extends StatefulWidget {
  PowerScreen({Key key}) : super(key: key);

  @override
  _PowerScreenState createState() => _PowerScreenState();
  
  Widget createPowerScreen(
          {Color color,
          int maxLevel,
          int numLimit,
          int numLimitPow,
          String operatorStr,
          String gameMode}) =>
      BlocProvider<PowerBloc>(
          create: (context) => PowerBloc(PowerInitial(
              color: color,
              maxLevel: maxLevel,
              numLimit: numLimit,
              numLimitPow: numLimitPow,
              operatorStr: operatorStr,
              gameMode: gameMode)),
          child: PowerScreen());
}

class _PowerScreenState extends State<PowerScreen> {
  PowerBloc _powerBloc;

  @override
  void dispose() {
    _powerBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _powerBloc = BlocProvider.of<PowerBloc>(context);

    return StandartApp(
      onWillPop: () {
        runApp(HomeScreen());
      },
      child: BlocBuilder<PowerBloc, PowerState>(builder: (context, state) {
        return buildPowerScreen(state);
      }),
    );
  }

  int maxLevel, score = 0, numLimit;
  Color color, colorInput;
  String exercise = '', userInput, operatorStr, gameMode;

  Widget buildPowerScreen(PowerState state) {
    print(state);

    if (state is PowerInitial) {
      gameMode ??= state.gameMode;
      maxLevel ??= state.maxLevel;
      color ??= state.color;
      colorInput = color;
      numLimit ??= state.numLimit;
      operatorStr ??= state.operatorStr;
      userInput = '0';
      _initiatePower(
          numLimit: numLimit, operatorString: operatorStr, maxLevel: maxLevel);
    }

    if (state is ShowExercise) {
      exercise = state.exercise;
    }

    if (state is ShowPowerUserInput) {
      userInput = state.userInput;
    }

    if (state is ShowAnswer) {
      userInput = state.correctAnswer;
      colorInput = state.colorInput;
      _newExercise(
          numLimit: numLimit,
          operatorString: operatorStr,
          colorInput: colorInput,
          maxScore: state.maxScore);
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        (gameMode == 'Normal')?Progressbar(
          color: color,
          value: (score / maxLevel).toDouble(),
          padding: 8,
        ):PracticeScoreDisplay(score:score),
        SizedBox(
          height: 100,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DefaultTextStyle.merge(
              child: Math.tex(r'' + exercise, mathStyle: MathStyle.display),
              style: TextStyle(
                fontSize: 40,
                color: color,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            DefaultTextStyle.merge(
              child: Math.tex(r'\sf { ' + userInput + '}'),
              style: TextStyle(
                fontSize: 40,
                color: (colorInput),
              ),
            ),
          ],
        ),
        Expanded(
          child: Container(),
        ),
        KeyboardBloc(
          numberFunction: _changeValue,
          buttons: [
            [7, 8, 9],
            [4, 5, 6],
            [1, 2, 3],
            ['Del', 0, 'Ent']
          ],
          color: color,
          stringFunctions: [
            _deleteChar,
            _submit,
          ],
        ),
      ],
    );
  }

  void _initiatePower({int numLimit, String operatorString, int maxLevel}) {
    _powerBloc.add(InitiatePower(
        numLimit: numLimit, operatorStr: operatorString, maxLevel: maxLevel));
  }

  void _deleteChar(String value) {
    _powerBloc.add(DeletePowerValue());
  }

  void _changeValue(String number) {
    _powerBloc.add(ChangePowerValue(number));
  }

  void _submit(String value) {
    _powerBloc.add(SubmitPowerValue());
  }

  void _newExercise(
      {int numLimit,
      String operatorString,
      Color colorInput,
      bool maxScore}) async {
    if(gameMode == 'Normal'){
      if (colorInput == color_right[theme]) {
        score++;
      }
      if (maxScore) {
        GameFunctions().nextGameLevels(powerLevelList);
      }
    }else if (gameMode == 'Practice'){
      if (colorInput == color_right[theme]) {
        score++;
      }
      else{
        runApp(LevelLayout(trainingmode: powerPracticeList));
      }
    }

    await Future.delayed(Duration(seconds: 3));
    _powerBloc.add(GeneratePowerExercise());
    return;
  }
}
